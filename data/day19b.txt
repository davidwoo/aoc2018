00) addi 2 16 2 //jump to 17

01) seti 1 0 4  //r4 = 1  
for r4 = 1; r4 <= r3; ++r4 {

    02) seti 1 5 5  //r5 = 1


    for r5 = 1; r5 <= r3; ++r5 {
        03) mulr 4 5 1  //r1 = r4 * r5

        04) eqrr 1 3 1  //if r1 == r3const {
        05) addr 1 2 2  //  r0 += r4
        06) addi 2 1 2  //
        07) addr 4 0 0  //} 
    
    
        //08) addi 5 1 5  //r5 += 1
        //09) gtrr 5 3 1  //if r5 > r3 {}
        //10) addr 2 1 2  // jmp to 12
        //11) seti 2 6 2  //else jmp to 3
    }
    //12) addi 4 1 4  //r4 += 1

}
13) gtrr 4 3 1  //if r4 > r3        //quit if r4 > r3
14) addr 1 2 2  // jmp to 16
15) seti 1 7 2  //else jump to 2  

16) mulr 2 2 2  //QUIT jmp to 36


//CALC const for r3 (974 or 10,551,374)
17) addi 3 2 3  //r3 += 2 //assume starts 0
18) mulr 3 3 3  //r3 = 4
19) mulr 2 3 3  //r3 = 4 * 19  = 76
20) muli 3 11 3 //r3 *= 11 = 836
21) addi 1 6 1  //r1 = 6
22) mulr 1 2 1  //r1 *= 22 = 132 
23) addi 1 6 1  //r1 += 6  = 138
24) addr 3 1 3  //r3 += r1 = 974,

25) addr 2 0 2  //if r0 == 0 {
26) seti 0 3 2  // jmp to 1

27) setr 2 3 1  //r1 = 27
28) mulr 1 2 1  //r1 = r1 * r2 = 27 * 28 = 756
29) addr 2 1 1  //r1 = r2 + r1 = 29 + 756 = 785
30) mulr 2 1 1  //r1 = r2 * r1 = 30 * 785 = 23550
31) muli 1 14 1 //r1 = r1 * 14 = 329700
32) mulr 1 2 1  //r1 = r1 * 32 = 10,550,400
33) addr 3 1 3  //r3 = r3 + r1 = 10,551,374
34) seti 0 9 0   //r0 = 0
35) seti 0 5 2   //jmp to 1 