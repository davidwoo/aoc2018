1929 units each with 13168 hit points weak:(bludgeoning) with an attack that does 13 fire damage at initiative 7
2143 units each with 14262 hit points immune:[radiation] with an attack that does 12 fire damage at initiative 10
1380 units each with 20450 hit points weak:(slashing, radiation) immune:[bludgeoning, fire] with an attack that does 28 cold damage at initiative 12
4914 units each with 6963 hit points weak:(slashing) immune:[fire] with an attack that does 2 cold damage at initiative 11
1481 units each with 14192 hit points weak:(slashing, fire) immune:[radiation] with an attack that does 17 bludgeoning damage at initiative 3
58 units each with 40282 hit points weak:(cold, slashing) with an attack that does 1346 radiation damage at initiative 9
2268 units each with 30049 hit points immune:[cold, slashing, radiation] with an attack that does 24 radiation damage at initiative 5
3562 units each with 22067 hit points with an attack that does 9 fire damage at initiative 18
4874 units each with 37620 hit points weak:(cold) immune:[bludgeoning] with an attack that does 13 bludgeoning damage at initiative 1
4378 units each with 32200 hit points weak:(cold) with an attack that does 10 bludgeoning damage at initiative 2