//failed experiment in using lifetimes
struct Iter<'a>{
    index : usize,
    list: &'a mut [Node],
}

impl Iter
{
    fn new<'a>( list: &'a mut [Node]) -> Iter<'a>
    {
        Iter{index:0, list}
    }

    fn move_forwards(&self, count: usize)->Iter
    {
        let mut new_index = self.index;
        for _ in 0..count{
            new_index = self.list[new_index].next;
        }
        Iter{index: new_index, list:self.list} 
    }

    fn move_back(&self, count: usize)->Iter
    {
        let mut new_index = self.index;
        for _ in 0..count{
            new_index = self.list[new_index].prev;
        }
        Iter{index: new_index, list:self.list}
    }

    fn insert_after(&self, value: usize)->Iter
    {
        let next_index = self.list[self.index].next;
        self.list[self.index].next = value;
        self.list[next_index].prev = value;
        self.list[value].prev = self.index;
        self.list[value].next = next_index;

        Iter{index:value, list:self.list} 
    }

    fn remove(&self)->Iter
    {
        let prev_index = self.list[self.index].prev;
        let next_index = self.list[self.index].next;

        self.list[prev_index].next = next_index;
        self.list[next_index].prev = prev_index;
        self.list[self.index].prev = self.index;
        self.list[self.index].next = self.index;

         Iter{index:next_index, list:self.list} 
    }

    fn value(&self)->usize
    {
        self.list[self.index].value
    }
}