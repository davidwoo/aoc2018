
use std::io;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::error::Error;

pub fn answer1()
{
    let path = Path::new("data/day8.txt");
    let mut file = match File::open(&path){
        Err(why) => panic!("couldn't open {}:{}", path.display(), why.description()),
        Ok(file) => file,
    };

    let mut contents = String::new();
    file.read_to_string(&mut contents);

    let numbers :Vec<i32> = contents.split_whitespace().map(|s| s.parse::<i32>().unwrap() ).collect();
    println!("Hello WOrld");

    let mut child_stack = Vec::new();
    let mut meta_stack = Vec::new();

    child_stack.push(1);
    meta_stack.push(0);

    let mut accumulator = 0;
    let mut num_it = numbers.iter();
    while !child_stack.is_empty() {

        match child_stack.last().unwrap() {
            0 =>{
                //POP top
                child_stack.pop();
                //POP META
                let meta_count = meta_stack.pop().unwrap();
                //READ META
                for _ in 0..meta_count {
                    accumulator += num_it.next().unwrap();
                }
            },
            _=>{
            //dec top
            *child_stack.last_mut().unwrap() -= 1;
                //read child count, meta onto stacks
            child_stack.push( *num_it.next().unwrap() );
            meta_stack.push( *num_it.next().unwrap() );
            }
        }

     
    }

    println!("The total is {}", accumulator);


}


pub fn answer2()
{
    let path = Path::new("data/day8.txt");
    let mut file = match File::open(&path){
        Err(why) => panic!("couldn't open {}:{}", path.display(), why.description()),
        Ok(file) => file,
    };

    let mut contents = String::new();
    file.read_to_string(&mut contents);

    let numbers :Vec<i32> = contents.split_whitespace().map(|s| s.parse::<i32>().unwrap() ).collect();
    println!("Hello WOrld");

    let mut child_stack = Vec::new();
    let mut meta_stack = Vec::new();
    let mut value_stack = Vec::new();
 

    let mut final_value= 0;
  //  let mut accumulator = 0;
    let mut num_it = numbers.iter();

    child_stack.push(*num_it.next().unwrap());
    meta_stack.push(*num_it.next().unwrap());
    value_stack.push( Vec::new() );
    while !child_stack.is_empty() {

        match child_stack.last().unwrap() {
            0 =>{
                //POP top
                child_stack.pop();
                //POP META
                let meta_count = meta_stack.pop().unwrap();
                let values = value_stack.pop().unwrap();
                let mut node_value = 0;
                //READ META values and use values
                for _ in 0..meta_count {

                    let node_index: i32 = *num_it.next().unwrap() -1;
                    let sub_value = match values.get( node_index as usize )
                    {
                        None=>0,
                        Some(&x)=>x,
                    };

                    node_value += sub_value;
                }
                if !value_stack.is_empty() {
                value_stack.last_mut().unwrap().push(node_value);
                }
                else
                {
                    final_value =node_value;
                }
            },
            _=>{
            //dec top
            *child_stack.last_mut().unwrap() -= 1;
                //read child count, meta onto stacks
                let child_count = *num_it.next().unwrap();
                let meta_count = *num_it.next().unwrap();

                if child_count == 0{ 
                     //READ META
                     let mut accumulator = 0;
                    for _ in 0..meta_count {
                        accumulator += num_it.next().unwrap();
                    }
                    //read meta data now and evaluate
                    //push onto the value stack top
                    value_stack.last_mut().unwrap().push(accumulator);
                }
                else 
                {
                    // push the child and metadata stack 
                   child_stack.push( child_count );
                     meta_stack.push( meta_count );
                    value_stack.push( Vec::new() );
                }
            }
        }

     
    }

    println!("The total is {}", final_value);


}