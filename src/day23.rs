
use crate::input;
use regex::Regex;
use std::i64;
use std::cmp;
use std::cmp::Ordering;
use std::collections::BinaryHeap;

type Pos = (i64, i64, i64);

#[derive(PartialEq, Eq, Copy, Clone)]
struct Bot
{
    pos: Pos,
    r: i64,
}



#[derive(PartialEq, Eq, Clone)]
struct Cube{
    
    min: Pos,
    side: i64,
    bots: usize,
}

impl Cube{

    fn size(&self) ->i64
    {
        self.side
    }

    fn bot_count(&self)->usize
    {
        self.bots
    }
    fn set_bot_count(&mut self, count:usize)
    {
        self.bots = count
    }

    fn distance(&self)->usize{
        let pos = self.pos();
        (pos.0.abs() + pos.1.abs() + pos.2.abs()) as usize
    }

    fn pos(&self)->Pos{
       // let (x,y,z) = self.min;
       // let half_size = (self.side>>1) as i64;
       // (
       //     x+ half_size,
       //     y+half_size,
       //     z+half_size,
      //  )
      self.min()
    }

    fn min(&self)->Pos{
        self.min     
    }

    fn max(&self)->Pos{
        let (x,y,z) = self.min;
        let side = self.side;
        (
            x+side-1,
            y+side-1,
            z+side-1,
        )
    }

    fn new(min:Pos, side: i64)->Cube{

        Cube{
            min,
            side,
            bots: 0,
        }
    }

    fn neighbours(&self)->Vec<Cube>{
        let mut octants = Vec::new();

        //half the size
        let half_size = self.side / 2 ;
        let hs = half_size ;
        let (x,y,z) = self.min();

        //x,y,z
        octants.push( Cube::new((x,y,z), half_size) );
        //x+s, y,z
        octants.push( Cube::new((x+hs,y,z), half_size) );
        //x, y+s, z,
        octants.push( Cube::new((x,y+hs,z), half_size) );
        //x, y, z+s
        octants.push( Cube::new((x,y,z+hs), half_size) );
        //x+s, y+s, z
        octants.push( Cube::new((x+hs,y+hs,z), half_size) );
        //x+s, y, z+s
        octants.push( Cube::new((x+hs,y,z+hs), half_size) );
        //x, y+s, z+s
        octants.push( Cube::new((x,y+hs,z+hs), half_size) );
        //x+s, y+s, z+s
        octants.push( Cube::new((x+hs,y+hs,z+hs), half_size) );

        octants
    }

    fn overlaps_bot_range(&self, bot:&Bot)->bool
    {
        let mut overlaps = true;

        let cube_min = self.min();
        let cube_max = self.max();
        let bot_min = bot.min();
        let bot_max = bot.max();


        //approximate bot range with aabb - its wrong but may still work - NO

        if bot_min.0 >= cube_max.0 || bot_max.0 < cube_min.0 ||
            bot_min.1 >= cube_max.1 || bot_max.1 < cube_min.1 ||
            bot_min.2 >= cube_max.2 || bot_max.2 < cube_min.2 
        { 
            overlaps = false;
        }

        overlaps
    }


}

impl Ord for Cube {
    fn cmp(&self, other: &Cube) -> Ordering {

 //most bots
//then distance to origin
   //smaller cube is better
       
    
        
    match self.bot_count().cmp(&other.bot_count()) {
        
        Ordering::Equal => {
                match other.distance().cmp(&self.distance())
                {
                    Ordering::Equal =>{
                        other.size().cmp(&self.size()) 
                    },
                    cmp_value => cmp_value,
                }
            },
            cmp_value => cmp_value,
        }
            
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for Cube {
    fn partial_cmp(&self, other: &Cube) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn answer1()
{
    let lines = input::read_lines("data/day23.txt");

   
    let bot_pattern = Regex::new(r"^pos=<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>, r=([0-9]+)$").unwrap();
    let mut bots = Vec::new();

    for line in lines {

         if let Some(m) = bot_pattern.captures(&line) {

            bots.push( Bot{
                pos: ( 
                    m.get(1).unwrap().as_str().parse::<i64>().unwrap(),
                    m.get(2).unwrap().as_str().parse::<i64>().unwrap(), 
                    m.get(3).unwrap().as_str().parse::<i64>().unwrap() 
                    ),
                r:  m.get(4).unwrap().as_str().parse::<i64>().unwrap()
            } );
        }
    }


    let mb= bots.iter().max_by(|a,b| a.r.cmp(&b.r) ).unwrap();
    println!("The max range is {}", mb.r);

    
    
    {
        let is_in_range = |p: && Bot| {
            (mb.pos.0 - p.pos.0).abs() + 
            ( mb.pos.1 - p.pos.1).abs() + 
            (mb.pos.2 - p.pos.2).abs() <= mb.r as i64
            }
            ;
            
        let num_in_range = bots.iter().filter(is_in_range).count();

        let minmax = 
        bots.iter().fold( [0i64; 6], |mut mm, bot| {
            mm[0] = cmp::min( mm[0], bot.pos.0 - bot.r as i64);
             mm[1] = cmp::max( mm[1], bot.pos.0 + bot.r as i64);
              mm[2] = cmp::min( mm[2], bot.pos.1 - bot.r as i64);
               mm[3] = cmp::max( mm[3], bot.pos.1 + bot.r as i64);
                mm[4] = cmp::min( mm[4], bot.pos.2 - bot.r as i64);
                 mm[5] = cmp::max( mm[5], bot.pos.2 + bot.r as i64);
            mm
        });
    /*
        let num_in_range = bots.iter().filter(|p| {
            (mb.pos.0 - p.pos.0).abs() + 
            ( mb.pos.1 - p.pos.1).abs() + 
            (mb.pos.2 - p.pos.2).abs() <= mb.r as i64
            }).count();
            */



        println!("The num in range is {} {:?}", num_in_range, minmax);
     
     let start_size = 1 << 30;
     let sp:i64 = -(1 << 29);

     let mut start_cube = Cube::new((sp,sp,sp), start_size);
     let bot_count = bots.iter().filter(|b| b.in_range(&start_cube)).count();
     start_cube.set_bot_count(bot_count);
    // for bot in &bots{
    //     if start_cube.overlaps_bot_range(bot) {
    //        start_cube.add_bot(bot);
    //     }
    // }

     let mut queue = BinaryHeap::new();

    
     //start with cube -2  ^ 29 , + 2^29
     //then subdivide seeing which contains the most bots

        //stick cube in queue
         queue.push(start_cube);

        //while cube is not empty
        while !queue.is_empty() {

            //pop cube
            let cube = queue.pop().unwrap();

            if cube.size() == 1
            {
                let pos = cube.pos();
                let md = pos.0.abs() + pos.1.abs() + pos.2.abs();
                println!("The cube pos is {:?} distance {}", cube.pos(), md);
            
                break;
            }
               

            //generate 8 new cubes
            let neighbours = cube.neighbours();

            //for each cube
            for mut subcube in neighbours
            {
                if subcube.pos() == (12,12,12) {
                    let y = 10;
                }

                let bot_count = bots.iter().filter(|b| b.in_range(&subcube)).count();
                subcube.set_bot_count(bot_count);

                if subcube.bot_count() > 0 
                {
                    queue.push(subcube);
                }
            } 
                 
        }
    }
}

impl Bot
{
    fn min(&self)->Pos{
        (self.pos.0-self.r ,
        self.pos.1-self.r ,
        self.pos.2-self.r )
    }

    fn max(&self)->Pos{
        (self.pos.0+self.r ,
        self.pos.1+self.r ,
        self.pos.2+self.r )
    }
    fn in_range(&self, cube: &Cube)->bool{
      
        let cube_min = cube.min();
        let cube_max = cube.max();
        let pos = self.pos;

  let mut d = 0;
        if pos.0 > cube_max.0  { d += pos.0 -cube_max.0; }
        if pos.0 < cube_min.0  { d += cube_min.0 - pos.0 ; }
        if pos.1 > cube_max.1  { d += pos.1 -cube_max.1; }
        if pos.1 < cube_min.1  { d += cube_min.1 - pos.1 ; }
        if pos.2 > cube_max.2  { d += pos.2 -cube_max.2; }
        if pos.2 < cube_min.2  { d += cube_min.2 - pos.2; }

        let is_in_range = d <= self.r ;
        is_in_range
    }
}
