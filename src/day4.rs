
use std::cmp::Ordering;
use std::collections::HashMap;

use super::input;
use super::chrono;
use super::chrono::Timelike;


#[derive(Eq)]
enum GuardAction
{
    BeginsShift(i32),
    WakesUp,
    FallsAsleep,
}


impl PartialEq for GuardAction {
    fn eq(&self, other: &GuardAction) -> bool {
        self == other
    }
}


#[derive(Eq)]
struct GuardInfo
{
    time: i64,
    min: u32,
    action: GuardAction,
}

impl GuardInfo{
    pub fn new( line: &mut str)->GuardInfo
    {
        let (time, rest) = line.split_at_mut(18);
        let dt = chrono::NaiveDateTime::parse_from_str(time, "[%Y-%m-%d %H:%M]").unwrap();

        let mut it = rest.split_whitespace();
        let action_str = it.next().unwrap();
        let action = match action_str {
            "wakes"=> GuardAction::WakesUp,
            "Guard"=> { 
                 let id_str = it.next().unwrap();
                 let short = &id_str[1..];
                let id = short.parse::<i32>().unwrap();
                GuardAction::BeginsShift(id)
            },
            _=> GuardAction::FallsAsleep,
        };

       
        GuardInfo{ 
            time:dt.timestamp(), 
            min: dt.time().minute(),
            action
            }
    }
}

impl Ord for GuardInfo
{
    fn cmp(&self, other: &GuardInfo) -> Ordering {
        self.time.cmp(&other.time)
    }
}

impl PartialOrd for GuardInfo {
    fn partial_cmp(&self, other: &GuardInfo) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for GuardInfo {
    fn eq(&self, other: &GuardInfo) -> bool {
        self.time == other.time
    }
}


struct SleepData
{
    total_time : u32,
    min_asleep : [i32;60],
}

    fn load_guard_data()-> Vec<GuardInfo>
    {
        let mut guard_data = Vec::new();

        let lines = input::read_lines("data/day4.txt");
        for mut line in lines {
            guard_data.push( GuardInfo::new( &mut line ) )
        }

        guard_data.sort();

        guard_data
    }

    pub fn answer1()
    {
        //read in input
        let mut guard_data = load_guard_data();

      

        let mut guard_sleep_map : HashMap<i32,SleepData> = HashMap::new();
        
        //run through the guard actions

        let mut current_id : i32 = 0;
        let mut current_time_mins_after_midnight : u32 = 0;
        for gi in guard_data{   

                match gi.action {
                    GuardAction::FallsAsleep=>{
                        //store current time
                        current_time_mins_after_midnight = gi.min;
                       
                    },
                    GuardAction::WakesUp=>{
                        //calc time asleep
                       // let time_asleep = gi.min-current_time_mins_after_midnight;
                        let mut si = guard_sleep_map.get_mut(&current_id);
                        let mut si = si.unwrap();
                      
                        for index in current_time_mins_after_midnight..gi.min {
                            let i = index as usize;
                            si.total_time += 1;
                            si.min_asleep[i] +=1;
                        }
                      
                      
                        //update map
                    },
                    GuardAction::BeginsShift(id)=>{
                        current_id = id;
                        match guard_sleep_map.get(&current_id)
                        {
                            Some(_)=>(),
                            None=>{guard_sleep_map.insert(current_id,SleepData{total_time:0, min_asleep:[0;60]} );},
                        }
                    },
                }
            
           
            //get a guard start create or find guard with id
        //get time asleep s
        //get time wake up add time total 
          
       
        }
       
        //run through map for guard with most time asleep
          let mut max_id:i32 = 0;
            let mut max_sleep:u32 = 0;
            for (k, v) in &guard_sleep_map {
                if v.total_time > max_sleep
                {
                    max_sleep = v.total_time;
                    max_id = *k;
                }
            }

     
        let lazy = guard_sleep_map.get(&max_id).unwrap();
        //now search for max time asleep
        let mut lazy_min_index = 0;
        let mut lazy_time = 0;
        for i in 0..60 {
            if lazy.min_asleep[i] > lazy_time
            {
                lazy_min_index = i;
                lazy_time =  lazy.min_asleep[i];
            }
        }

        println!("The guard who slept the most is  {}", max_id);
           println!("The the minute in which he slept the most is   {}", lazy_min_index);
             println!("Their product is   {}", lazy_min_index * max_id as usize);

    }

    pub fn answer2()
    {
        //read in input
        let guard_data = load_guard_data();
//for each minute find the guard who slept the most in it and how many times

        let mut minute_to_id_map : Vec<HashMap<i32, u32>>= Vec::new();
        for _ in 0..60 
        {
            minute_to_id_map.push( HashMap::new() );
        }



  //run through the guard actions

        let mut current_id : i32 = 0;
        let mut current_time_mins_after_midnight : u32 = 0;
        for gi in guard_data
        {   

                match gi.action 
                {
                    GuardAction::FallsAsleep=>
                    {
                        //store current time
                        current_time_mins_after_midnight = gi.min;
                       
                    },
                    GuardAction::WakesUp=>
                    {
                      //for each minute asleep - update the count for that id
                      
                      
                        for index in current_time_mins_after_midnight..gi.min{
                            
                            let mut entry = minute_to_id_map[index as usize].entry(current_id).or_insert(0);
                            *entry += 1;
                        }
                      
                      
                        //update map
                    },
                    GuardAction::BeginsShift(id)=>
                    {
                        current_id = id; 
                    },
                }
            
           
            //get a guard start create or find guard with id
        //get time asleep s
        //get time wake up add time total 
        
          
       
        }

        let mut biggest_count:u32 = 0;
        let mut biggest_minute = 0;
        let mut biggest_id = 0;
        for (minute, map) in minute_to_id_map.iter().enumerate(){
            for (id, count) in map{
                if *count > biggest_count{
                    biggest_count = *count;
                    biggest_minute = minute;
                    biggest_id = *id;
                }
            }
        }

          println!("The guard who slept the most is  {}", biggest_id);
           println!("The the minute in which he slept the most is   {}", biggest_minute);
             println!("Their product is   {}", biggest_minute * biggest_id as usize);
//run through the sorted guard data create map of minute->map(id->count)
//find the minute with the biggest count
//print minute * idS
    }
