use input;
use std::collections::HashSet;
use std::collections::HashMap;

#[derive( PartialEq, Hash, Eq, Clone, Copy)]
enum OpCode
{
    Addr,
    Addi,
    Mulr,
    Muli,
    Banr,
    Bani,
    Borr,
    Bori,
    Setr,
    Seti,
    Gtir,
    Gtri,
    Gtrr,
    Eqir,
    Eqri,
    Eqrr,
}

fn do_op(opcode : &OpCode, input: [i32;4], mut regs : [i32;4]) ->[i32;4]
{
    match opcode 
    {
         OpCode::Addr=>{
           regs[input[3]as usize] = regs[input[1] as usize] + regs[input[2] as usize];  
         },
     OpCode::Addi=>{
             regs[input[3]as usize] = regs[input[1] as usize] + input[2];  
     },
     OpCode::Mulr=>{
          regs[input[3]as usize] = regs[input[1] as usize] * regs[input[2] as usize];  
     },
     OpCode::Muli=>{
           regs[input[3]as usize] = regs[input[1] as usize] * input[2];  
     },
     OpCode::Banr=>{
            regs[input[3]as usize] = regs[input[1] as usize] & regs[input[2] as usize];  
     },
     OpCode::Bani=>{
         regs[input[3]as usize] = regs[input[1] as usize] & input[2];
     },
    OpCode::Borr=>{
          regs[input[3]as usize] = regs[input[1] as usize] | regs[input[2] as usize]; 
    },
    OpCode::Bori=>{
         regs[input[3]as usize] = regs[input[1] as usize] | input[2];
    },
     OpCode::Setr=>{
          regs[input[3]as usize] = regs[input[1] as usize];
     },
     OpCode::Seti=>{
         regs[input[3]as usize] = input[1];
     },
     OpCode::Gtir=>{
        regs[input[3]as usize] = if input[1]  > regs[input[2] as usize] {1} else {0}; 
     },
     OpCode::Gtri=>{
          regs[input[3]as usize] = if regs[input[1] as usize] > input[2]  {1} else {0}; 
     },
     OpCode::Gtrr=>{
      regs[input[3]as usize] = if regs[input[1] as usize]  > regs[input[2] as usize] {1} else {0}; 
     },
     OpCode::Eqir=>{
          regs[input[3]as usize] = if input[1]  == regs[input[2] as usize] {1} else {0}; 
     },
     OpCode::Eqri=>{
           regs[input[3]as usize] = if regs[input[1] as usize] == input[2]  {1} else {0}; 
     },
     OpCode::Eqrr=>{
           regs[input[3]as usize] = if regs[input[1] as usize]  == regs[input[2] as usize] {1} else {0};
     },
    }

    regs
}

struct Sample{
    before : [i32 ; 4],
    after : [i32 ; 4],
    instructions : [i32;4],
}

fn parse_instructions(inst_str: &String)->[i32;4]
{
    let mut instructions = [0,0,0,0,];

    for (i,m) in inst_str.split_whitespace().enumerate(){
        instructions[i] = m.parse().unwrap();
    }

    instructions
}

fn parse_registers(reg_string:&String)->[i32;4]{

    let mut registers = [0,0,0,0,];  

    let poss = reg_string.find('[').unwrap()+1;
    let pose = reg_string.find(']').unwrap();
    let numbers = reg_string.get(poss..pose).unwrap();
    for (i,m) in numbers.split(',').enumerate(){
        registers[i] = m.trim().parse().unwrap();
    }

    registers
}

fn build_samples(lines: Vec<String>)-> Vec<Sample>
{
    let mut samples = Vec::new();

    for chunk in lines.chunks(4){
        let before_str = &chunk[0];
        let inst_str = &chunk[1];
        let after_str = &chunk[2];

        let before = parse_registers(before_str);
        let after = parse_registers(after_str);
        let instructions = parse_instructions(inst_str);

        samples.push(
            Sample{
                before,
                after,
                instructions,
            }
        );
    }

    samples
}

pub fn answer1()
{

        let opcodes = [     
            OpCode::Addr,
            OpCode::Addi,
            OpCode::Mulr,
            OpCode::Muli,
            OpCode::Banr,
            OpCode::Bani,
            OpCode::Borr,
            OpCode::Bori,
            OpCode::Setr,
            OpCode::Seti,
            OpCode::Gtir,
            OpCode::Gtri,
            OpCode::Gtrr,
            OpCode::Eqir,
            OpCode::Eqri,
            OpCode::Eqrr,
        ];


    //read in samples
    let lines = input::read_lines("data/day16.txt");

    let samples = build_samples(lines);

    //let mut matching_samples_count = 0;
    let mut possibles = Vec::new();
    possibles.reserve(16);
    for (op_index, opcode) in opcodes.iter().enumerate() {
               
        let mut poss_values_for_opcode = HashSet::new();
        for sample in samples.iter()
        {
        
                let output = do_op( opcode, sample.instructions, sample.before);
                
                //if sample output = actual output then increment the possible count
                if output == sample.after {
                    poss_values_for_opcode.insert(sample.instructions[0]);
                }

        }

        if poss_values_for_opcode.len() == 1 {
            println!("hello world");
        }
        possibles.push( (op_index, poss_values_for_opcode) );
    }


    let mut opcode_value_map = [16;16];
    //loop until possibles is empty
    while !possibles.is_empty() {
        //find a set with only 1 value

        let mut item_found = false;
        let mut i = 0;
        while i != possibles.len() {
            if  possibles[i].1.len() ==1 {
                let val = possibles.remove(i);//remove the item from the vector

                let (op_index,set) = val;
        
               

                 //set hash map[opcodes[index]]=value
                let value = set.iter().next().unwrap();
                opcode_value_map[*value as usize] = op_index;
                //remove value from each of the possible sets
                for thing in possibles.iter_mut() {
                    thing.1.remove(value);
                }
                item_found = true;
                break;
            } else {
                i += 1;
            }
        }

        assert_eq!(item_found, true);

    }


    //read in and parse the instructions
    let mut instructions = Vec::new();
    let inst_lines = input::read_lines("data/day16b.txt");
    for line in inst_lines{
        instructions.push( parse_instructions(&line));
    }

    let mut registers = [0;4];
    //for each instruction
    for instruction in instructions 
    {
        //convert instruction 0 to opcode
        let op = opcodes[ opcode_value_map[ instruction[0] as usize] ];
        registers = do_op(&op, instruction, registers);
    }


    println!("Finished {}", registers[0]);



    //println!("Number of samples matching >3 == {}", matching_samples_count);
}