
use input;
use std::cmp::Ordering;
use std::str;
use std::collections::HashMap;


#[derive(Clone)]
struct Truck
{
    pos_x : i32,
    pos_y : i32,
    dir: Direction,
    next_turn : TurnChoice,
    crashed : bool,
}

impl Truck
{

    fn new(pos_x : i32, pos_y:i32, dir: Direction)->Truck
    {
        Truck{
            pos_x,
            pos_y,
            dir,
            next_turn : TurnChoice::Left,
            crashed : false,
        }
    }

    fn update(&self, grid: &Vec<Vec<GridState>>)->Truck
    {
        let mut next = self.clone();
        /*
        //move on grid
        */

        match &grid[self.pos_y as usize][self.pos_x as usize] 
        {
            GridState::LR=>{
                match self.dir {

                    Direction::Left =>{
                        next.pos_x -= 1;
                    },
                    _=> {
                        next.pos_x += 1;
                    },
                }
            },
            GridState::UD=>{
                match self.dir {

                    Direction::Up =>{
                        next.pos_y -= 1;
                    },
                    _=> {
                        next.pos_y += 1;
                    },
                }
            },  //|
            GridState::UR=>{
                match self.dir {

                    Direction::Up =>{
                        next.pos_x += 1;
                        next.dir = Direction::Right;
                    },
                    Direction::Down =>{
                        next.pos_x -= 1;
                        next.dir = Direction::Left;
                    },
                    Direction::Left=> {
                        next.pos_y += 1;
                        next.dir = Direction::Down;
                    },
                    Direction::Right=> {
                        next.pos_y -= 1;
                        next.dir = Direction::Up;
                    },
               
                }       
            },  // /
            GridState::DR=>{

                match self.dir {
                    Direction::Up =>{
                        next.pos_x -= 1;
                        next.dir = Direction::Left;
                    },
                    Direction::Down =>{
                        next.pos_x += 1;
                        next.dir = Direction::Right;
                    },
                    Direction::Left=> {
                        next.pos_y -= 1;
                        next.dir = Direction::Up;
                    },
                    Direction::Right=> {
                        next.pos_y += 1;
                        next.dir = Direction::Down;
                    },
                };
            }, // \
            GridState::C=>{
                match self.next_turn{

                    TurnChoice::Left=>{
                        next.next_turn = TurnChoice::Straight;

                        match self.dir {
                            Direction::Up =>{
                                next.pos_x -= 1;
                                next.dir = Direction::Left;
                            },
                            Direction::Down =>{
                                next.pos_x += 1;
                                next.dir = Direction::Right;
                            },
                            Direction::Right=> {
                                next.pos_y -= 1;
                                next.dir = Direction::Up;
                            },
                            Direction::Left=> {
                                next.pos_y += 1;
                                next.dir = Direction::Down;
                            },
                        }
                    },

                    TurnChoice::Straight=>{
                        next.next_turn = TurnChoice::Right;

                        match self.dir {
                            Direction::Up =>{
                                next.pos_y -= 1;
                                next.dir = Direction::Up;
                            },
                            Direction::Down =>{
                                next.pos_y += 1;
                                next.dir = Direction::Down;
                            },
                            Direction::Left=> {
                                next.pos_x -= 1;
                                next.dir = Direction::Left;
                            },
                            Direction::Right=> {
                                next.pos_x += 1;
                                next.dir = Direction::Right;
                            },
                        }
                    },

                    TurnChoice::Right=>{
                        next.next_turn = TurnChoice::Left;

                        match self.dir {
                            Direction::Up =>{
                                next.pos_x += 1;
                                next.dir = Direction::Right;
                            },
                            Direction::Down =>{
                                next.pos_x -= 1;
                                next.dir = Direction::Left;
                            },
                            Direction::Left=> {
                                next.pos_y -= 1;
                                next.dir = Direction::Up;
                            },
                            Direction::Right=> {
                                next.pos_y += 1;
                                next.dir = Direction::Down;
                            },
                        }
                    },

                }
            },   // +
            _=>(),
        };

        next

    }

   
}

impl PartialEq for Truck {
    fn eq(&self, other: &Truck) -> bool {
        self.pos_x == other.pos_x && 
        self.pos_y == other.pos_y
    }
}
impl Eq for Truck {}

impl Ord for Truck
{
    fn cmp(&self, other: &Truck) -> Ordering {
        if self.pos_y < other.pos_y
        {
            Ordering::Less
        }
        else if  self.pos_y == other.pos_y
        {
            if self.pos_x < other.pos_x
            {
                Ordering::Less
            }
            else if  self.pos_x == other.pos_x
            {
                Ordering::Equal
            }
            else
            {
                Ordering::Greater
            }
        }
        else
        {
            Ordering::Greater
        }

     
    }
}

impl PartialOrd for Truck {
    fn partial_cmp(&self, other: &Truck) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}



enum GridState
{
    LR,  //-
    UD,  //|
    UR,  // /
    DR,  // \
    C,   // +
    EMPTY,
}


#[derive(Clone)]
enum TurnChoice
{
    Left,
    Straight,
    Right
}

#[derive(Clone)]
enum Direction
{
    Left,
    Right,
    Up,
    Down,
}

fn test_for_collisions(trucks: &Vec<Truck>)->Vec<(i32,i32)>
{
    let mut pos_to_count_map = HashMap::new();

    for truck in trucks {
        let pos = (truck.pos_x, truck.pos_y);
        let count = pos_to_count_map.entry( pos ).or_insert(0);
        *count += 1;
       
    }

    let mut crash_points = Vec::new();
    for (pos,count) in pos_to_count_map{
        if count > 1
        {
            crash_points.push(pos);
        }
    }
 
    crash_points
}


fn draw_grid(grid: &Vec<Vec<GridState>>, trucks: &Vec<Truck>)
{
    let mut lines: Vec<Vec<char>> = Vec::new();

    for grid_line in grid {

        let mut bob = Vec::new();

        for state in grid_line {
            bob.push(
            match state {
                  GridState::LR=>'-',  //-
                 GridState::UD=>'|',  //|
                 GridState::UR=>'/',  // /
                 GridState::DR=>'\\',  // \
                 GridState::C=>'+',   // +
                 GridState::EMPTY=>' ', 
            }
            );
        }

        lines.push(bob);
    }

    for truck in trucks {
        lines[truck.pos_y as usize][truck.pos_x as usize] = 
        match truck.dir {
            Direction::Left=>'<',
            Direction::Right=>'>',
            Direction::Up=>'^',
            Direction::Down=>'v',
        }
    }
    

    for line in lines{
        let s: String = line.iter().collect();
        println!("{}", s) ;
    }
}
pub fn answer1()
{
    let lines = input::read_lines("data/day13.txt");

    let mut trucks = Vec::new();
    //for each line
    let mut grid = Vec::new();

    for (y ,line) in lines.iter().enumerate(){
    //for each byte
        let mut gridline = Vec::new();
        for (x ,token) in line.chars().enumerate() {
            gridline.push(
            match token 
            {
                '-'=>GridState::LR,
                '|'=>GridState::UD,
                '\\'=>GridState::DR,
                '/'=>GridState::UR,
                '+'=>GridState::C,
                '<'=>{
                    trucks.push( Truck::new(x as i32,y as i32, Direction::Left) );
                    GridState::LR
                },
                 '>'=>{
                    trucks.push( Truck::new(x as i32,y as i32, Direction::Right) );
                    GridState::LR
                }
                ,
                 '^'=>{
                    trucks.push( Truck::new(x as i32,y as i32, Direction::Up) );
                    GridState::UD
                },
                 'v'=>{
                    trucks.push( Truck::new(x as i32,y as i32, Direction::Down) );
                    GridState::UD
                }
                _=>GridState::EMPTY,
            });
        //add the state to the grid
        //if truck 
            //add the state to grid
            //add a truck to trucks
        }

        grid.push(gridline);

    }

    let grid = grid;

    let mut crash = (8,7);
    let mut has_crashed = false;
    let mut it_count = 0;
    while !has_crashed {

        //sort trucks
        trucks.sort();
        let mut next_trucks = trucks.clone();
        for (i,truck) in trucks.iter().enumerate()
        {
            
            if !truck.crashed && !next_trucks[i].crashed {
            //update truck
            next_trucks[i] = truck.update(&grid);

            
            let crash_points =  test_for_collisions(&next_trucks);
            
            
            if !crash_points.is_empty() {

                for t in next_trucks.iter_mut() {
                    if crash_points.contains( &( t.pos_x, t.pos_y)){
                        t.crashed = true;
                    }
                }
           
               
            }
            }
            
        }
    
        it_count += 1;

        next_trucks.retain(|t| !t.crashed );
        trucks = next_trucks;

        if trucks.len() == 1{
            has_crashed = true;
            crash.0 = trucks[0].pos_x;
            crash.1 = trucks[0].pos_y;
        }
        //println!("\nIteration {}", it_count);
        //draw_grid(&grid, &trucks);
    }
   

    println!("point of crash is ({},{})", crash.0, crash.1);
}