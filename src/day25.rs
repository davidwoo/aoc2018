
use crate::input;
use std::collections::HashSet;
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug)]
struct Star
{
    id: usize,
    pos: [i32;4],
} 

impl Star
{

}

fn build_stars( lines: Vec<String>)->Vec<Star>
{
    let mut stars = Vec::new();

    for (id,line) in lines.iter().enumerate(){

        let nums: Vec<i32> =  line.split(',').map(|n| n.parse::<i32>().unwrap()).collect();
        let mut pos = [0; 4];
        pos.copy_from_slice( &nums[..] );
        stars.push( Star{id,pos} );
    }
    stars
}

fn stars_connected(star1: &Star, star2: &Star)->bool
{
   let distance = i32::abs(star1.pos[0] - star2.pos[0]) + 
   i32::abs(star1.pos[1] - star2.pos[1]) + 
   i32::abs(star1.pos[2] - star2.pos[2]) +
   i32::abs(star1.pos[3] - star2.pos[3]);

   distance <= 3
}

#[derive(Debug)]
struct Link
{
    id: usize,
    stars: HashSet<usize>,
}

impl Link
{
    fn new(id: usize) ->Link
    {
        let mut stars = HashSet::new();
        stars.insert(id);
        Link{
          id,
          stars,
        }
    }
}

fn link_stars( links : &mut[Rc<RefCell<Link>>], id1 : usize, id2:usize)
{

    //if not already on the same list
    //if the link_ids are diffferent
    if !Rc::ptr_eq(&links[id1], &links[id2]) {

        let link1 = links[id1].clone();
        let link2 = links[id2].clone();

      //  let join : HashSet<usize> = link1.stars.union(&link2.stars).map(|&i|i).collect();

        for id in link2.borrow().stars.iter(){
            link1.borrow_mut().stars.insert(*id);
            links[*id] = link1.clone();
        }
       // link1.get_mut().stars = join;
    }
}

pub fn answer1()
{
    let lines = input::read_lines("data/day25.txt");

    let stars = build_stars(lines);

    let mut links : Vec<_> = stars.iter().map( |s| Rc::new(RefCell::new(Link::new(s.id)) )).collect();
    //links.iter().for_each(|l| println!("{:?}", l));

    let mut star_tail = stars.as_slice();
    while star_tail.len() > 0 
    {
        if let Some( (star_a,tail) ) = star_tail.split_first()
      {

        for star_b in tail{
           
            if stars_connected(star_a, star_b) {
                link_stars( &mut links, star_a.id, star_b.id );
                //println!("connected {:?} <=> {:?}", star_a, star_b);  
            }  
        }

        star_tail = tail;
      }
    }
   
    let num_constellations = links.iter().fold( HashSet::new(), |mut s,l| {s.insert(l.borrow().id); s} ).len();
    println!("The number of constellations is {}", num_constellations);
}