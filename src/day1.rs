

use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;
use std::cmp::Ordering;



fn positive_modulo( i:i64, d:i64)->i64
{
   (i % d + d) % d
}

#[derive(Eq)]
struct FreqPair
{
    freq: i64,
    index: usize,
}

impl Ord for FreqPair
{
    fn cmp(&self, other: &FreqPair) -> Ordering {
        self.freq.cmp(&other.freq)
    }
}

impl PartialOrd for FreqPair {
    fn partial_cmp(&self, other: &FreqPair) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for FreqPair {
    fn eq(&self, other: &FreqPair) -> bool {
        self.freq == other.freq
    }
}


    pub fn answer1()
    {

    }

    pub fn answer2()
    {
            let path = Path::new("data/frequencies.txt");
    let file = match File::open(&path){
        Err(why) => panic!("couldn't open {}:{}", path.display(), why.description()),
        Ok(file) => file,
    };
    let reader = BufReader::new(&file);


    let mut values = Vec::new();
    for( _, line) in reader.lines().enumerate(){
        let value = line.unwrap().parse::<i64>().unwrap();
        values.push( value);
    }

    let mut frequency_set = Vec::new();
    frequency_set.push(0);
    
    let mut accumulator = 0;
    let values_ref = &values;
 
         let mut equ_classes : Vec<Vec<FreqPair>> = Vec::with_capacity(values.len());
         equ_classes.push(Vec::new());
        
    for value in values_ref {
      
        accumulator += value;

        if frequency_set.len() < values.len() 
        {
            frequency_set.push(accumulator);
            equ_classes.push(Vec::new());
        }

 

    }

    let  resultant_frequency = accumulator;
    println!("The first repeated frequency is {}", resultant_frequency);


   
    //for each member of the frequency set
    //add to its thing in the equivalence class
    for (freq_index, &freq) in frequency_set.iter().enumerate() {
        let index = positive_modulo( freq, resultant_frequency) as usize;
        equ_classes[index].push(FreqPair{freq,index:freq_index});
    }
    
    let mut smallest_difference = i64::max_value();
    let mut first_repeated_frequency = 0;
    let mut lowest_index = usize::max_value();
    //let mut possibles = Vec::new();
    for mut class in equ_classes
    {
        if class.len() > 1
        {
            class.sort();
            let diff = class[1].freq - class[0].freq;
            if diff == smallest_difference {
                //we want the freq the 
                if class[0].index < lowest_index
                {
                    first_repeated_frequency = class[1].freq;
                    lowest_index = class[0].index;
                }
   
            }
            else if diff < smallest_difference
            {
                smallest_difference = diff;
                first_repeated_frequency = class[1].freq;
                lowest_index = class[0].index;

            }
        }
      
    }



    println!("The first repeated frequency is {}", first_repeated_frequency);

    }
