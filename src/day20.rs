use crate::input;

use std::ops::{Index, IndexMut};
use std::cmp;
use std::collections::HashMap;

struct Map{
    points : Vec<char>,
    width: usize,
    height: usize,
}

impl Map{
    fn new(in_width : usize, in_height: usize)->Map{

        let width = 1 + 2 * in_width;
        let height = 1 + 2 * in_height;
        let mut points = vec!['#'; width * height];

        Map{
            points,
            width,
            height,
        }

    }


    fn print(&self)
    {
        for chunk in self.points.chunks(self.width) {
            println!("{}", chunk.iter().collect::<String>());
        }
        println!();
    }
}

impl Index<Pos> for Map {
    type Output = char;

    fn index(&self, pos:Pos)->&char{
        &self.points[ pos.0 * self.width + pos.1 ]
    }

}
impl IndexMut<Pos> for Map {

    fn index_mut(&mut self, pos:Pos)->&mut char{
        &mut self.points[ pos.0  * self.width + pos.1  ]
    }

}

type Pos = (usize, usize);
struct Mapper {
    map: Map,
    start: Pos,
    current: Pos,
    stack: Vec<Pos>,
   
}

impl Mapper{
    fn new(instructions: &[char] )->Mapper{

        let mut n_max = 0;
        let mut s_max = 0;
        let mut e_max = 0;
        let mut w_max = 0;
        let mut pos: (i32,i32,usize) = (0,0,0);
        let mut pos_stack = Vec::new();
 //       let mut farthest_pos = pos;
        let mut pos_map = HashMap::new();
        let mut check_best_pos = |p:(i32,i32,usize)|{
            let entry = pos_map.entry( (p.0, p.1) ).or_insert(p.2);
            *entry = cmp::min(*entry, p.2);
        };
        for inst in instructions {
            match inst {
                '^' | '(' =>{ pos_stack.push(pos); }, //push
                '$' | ')' =>{ pos = pos_stack.pop().unwrap(); }, //pop
                '|'       =>{ pos = *pos_stack.last().unwrap(); }, //return to stack top
                'N'       =>{ pos.0 -= 1; pos.2 += 1; n_max = cmp::min(n_max,pos.0); check_best_pos(pos); },
                'E'       =>{ pos.1 += 1; pos.2 += 1; e_max = cmp::max(e_max,pos.1); check_best_pos(pos); },
                'W'       =>{ pos.1 -= 1; pos.2 += 1; w_max = cmp::min(w_max,pos.1); check_best_pos(pos); },
                'S'       =>{ pos.0 += 1; pos.2 += 1; s_max = cmp::max(s_max,pos.0); check_best_pos(pos); },
                _=>(),
            }
        }

        let minmax_doors = *pos_map.values().max().unwrap();

        let sp_count = pos_map.values().filter(|&v| *v>=1000).count();


        let height = (1 + s_max - n_max) as usize;
        let width = (1 + e_max - w_max) as usize;
   



        let mut map = Map::new( width , height );
        
        let start: Pos = ( (1- 2 * n_max) as usize, (1 - 2* w_max) as usize );
        map[start] = 'X';
        //let mid_point = map.width/2;
        //let start = (mid_point, mid_point);
        Mapper{
            map,
            start,
            current: start,
            stack: Vec::new(),
       
        }
    }


    fn move_north(&mut self)
    {
        let (r,c) = self.current; 
        let r1 = r - 1;
        let r2 = r - 2;

        self.map[ (r1, c)] = '-';
        self.map[ (r2, c)] = '.';        
        self.current.0 = r2;
    }
     fn move_south(&mut self)
    {
        let (r,c) = self.current; 
        let r1 = r + 1;
        let r2 = r + 2;

        self.map[ (r1, c)] = '-';
        self.map[ (r2, c)] = '.';        
        self.current.0 = r2;
    }
     fn move_east(&mut self)
    {
        let (r,c) = self.current; 
        let c1 = c + 1;
        let c2 = c + 2;

        self.map[ (r, c1)] = '|';
        self.map[ (r, c2)] = '.';        
        self.current = (r,c2);
    }

    fn move_west(&mut self)
    {
        let (r,c) = self.current; 
        let c1 = c - 1;
        let c2 = c - 2;

        self.map[ (r, c1)] = '|';
        self.map[ (r, c2)] = '.';        
        self.current = (r,c2);
    }

    fn push(&mut self){
        self.stack.push( self.current );
    }
    fn pop(&mut self){
        self.current = self.stack.pop().unwrap();
    }

    fn jump_back(&mut self)
    {
        self.current = *self.stack.last().unwrap();
    }

    fn build_map(&mut self , instructions: &[char])
    {
        for inst in instructions {
            match inst {
                '^' | '(' =>{self.push(); }, //push
                '$' | ')' =>{self.pop(); }, //pop
                '|'       =>{self.jump_back(); }, //return to stack top
                'N'       =>{self.move_north();},
                'E'       =>{self.move_east();},
                'W'       =>{self.move_west();},
                'S'       =>{self.move_south();},
                _=>(),
            }
        }
    }

    fn print_map(&self)
    {
        self.map.print();
    }
}

pub fn answer1()
{
    let instructions = input::read_chars("data/day20.txt");



    let mut mapper = Mapper::new(&instructions);

 
    mapper.build_map( &instructions );
    mapper.print_map();
}