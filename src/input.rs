use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;

#[allow(dead_code)]
pub fn read_lines( filename: &str ) -> Vec<String>
{
    let path = Path::new(filename);
    let file = match File::open(&path){
        Err(why) => panic!("couldn't open {}:{}", path.display(), why.description()),
        Ok(file) => file,
    };
    let reader = BufReader::new(&file);


    let mut values = Vec::new();
    for line in reader.lines() 
    {
        values.push( line.unwrap() );
    }
        
    values
    
}

#[allow(dead_code)]
pub fn read_bytes( filename: &str )->Vec<u8>
{
     let path = Path::new(filename);
    let file = match File::open(&path){
        Err(why) => panic!("couldn't open {}:{}", path.display(), why.description()),
        Ok(file) => file,
    };
    let reader = BufReader::new(&file);

    reader.bytes().map(|r| r.unwrap() ).collect::<Vec<u8>>()

}

#[allow(dead_code)]
pub fn read_chars( filename: &str )->Vec<char>
{
     let path = Path::new(filename);
    let file = match File::open(&path){
        Err(why) => panic!("couldn't open {}:{}", path.display(), why.description()),
        Ok(file) => file,
    };
    let reader = BufReader::new(&file);

    reader.bytes().map(|r| r.unwrap() as char ).collect()

}

