use std::usize;

struct Node{
    next : usize,
    prev : usize,
    value : usize,
}

struct NodeList
{
    nodes: Vec<Node>,
}


impl NodeList
{
    fn new(size: usize)->NodeList
    {
        let mut nodes = Vec::new();
        nodes.reserve(size+1);

        for i in 0..=size {
    
            nodes.push( Node{ next:i, prev:i, value:i}); 
        }

        NodeList{ nodes }
    }


    fn move_forwards( &self, pos: usize, count:usize)->usize
    {
        let mut new_index = pos;
        for _ in 0..count{
            new_index = self.nodes[new_index].next;
        }
       
        new_index
    }

    fn move_back(&self, pos: usize, count: usize)->usize
    {
        let mut new_index = pos;
        for _ in 0..count{
            new_index = self.nodes[new_index].prev;
        }
        new_index
    }

    fn insert_after(&mut self, pos: usize, value: usize)->usize
    {
        let next_index = self.nodes[pos].next;
        self.nodes[pos].next = value;
        self.nodes[next_index].prev = value;
        self.nodes[value].prev = pos;
        self.nodes[value].next = next_index;

        value
    }

    fn remove(&mut self, pos: usize)->usize
    {
        let prev_index = self.nodes[pos].prev;
        let next_index = self.nodes[pos].next;

        self.nodes[prev_index].next = next_index;
        self.nodes[next_index].prev = prev_index;
        self.nodes[pos].prev = pos;
        self.nodes[pos].next = pos;

         next_index
    }

    fn value(&self, pos:usize)->usize
    {
        self.nodes[pos].value
    }

}


pub fn answer1()
{
    //416 players
    //71975 points
   let num_players = 416;
   let high_marble = 7197500;


    let mut circle_list = NodeList::new(high_marble);
   
    let mut current :usize= 0;

 
   
    let mut player_scores = vec![0 ; num_players];

    for m in 1..=high_marble{

        if m % 23 == 0 {
            //give m as a score to player m % num_players - remember that 0 is num_players
            //remove place 7 back from current add to score
            let mut score = m;
            let remove_point = circle_list.move_back(current, 7);
            score += circle_list.value(remove_point);
            current = circle_list.remove(remove_point);
            //current = index of next of removed 

            player_scores[ m % num_players ] += score;
        }
        else
        {
            //get current +1
            let next1 = circle_list.move_forwards(current, 1);
        
            //get current + 2
            //insert value between
            current = circle_list.insert_after(next1, m );
            //current = top index
        }
    }


    //read out the highest score
    let mut highest_score = usize::min_value();
    for score in player_scores {
        if score > highest_score {
            highest_score = score;
        }
    }

    println!("The highest score is {}", highest_score);
}