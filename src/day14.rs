use std::collections::VecDeque;


fn calc_last_six(recipes: &Vec<u8>)->bool
{
    let mut last_six = false;
    let length = recipes.len();

    if length >= 6 {

        if recipes[length-1] == 1 && 
            recipes[length-2] == 3 &&
            recipes[length-3] == 2 &&
            recipes[length-4] == 0 && 
            recipes[length-5] == 4 &&
            recipes[length-6] == 4
        {
            last_six = true;
        }

    }
    else
    {
        last_six = false;
    }
    last_six
}
pub fn answer1()
{
    let target_count:usize = 440231;
   // let target_pattern:[u8;6] = [4,4,0,2,3,1];
    let mut recipe_count:usize = 0;
    let mut recipes: Vec<u8> = Vec::new();
    recipes.reserve(target_count + 10);
    recipes.push(3);
    recipes.push(7);

    let mut elf1_index = 0;
    let mut elf2_index = 1;

    let mut last_six = false;
    while !last_six  {

        let output = recipes[elf1_index] + recipes[elf2_index];

        if output < 10 {
            recipes.push(output);

            if output == 1 {
            last_six = calc_last_six(&recipes);
            }
        }
        else
        {
            recipes.push(1);
            last_six = calc_last_six(&recipes);

            if last_six {
                break;
            }

            let next_digit = output % 10;
            recipes.push(next_digit);

            if next_digit == 1{
            last_six = calc_last_six(&recipes);
            }
        }

        elf1_index = (elf1_index + recipes[elf1_index] as usize + 1) % recipes.len();
        elf2_index = (elf2_index + recipes[elf2_index] as usize + 1) % recipes.len();
    }

    for r in recipes.iter().skip(target_count) {
        print!("{}", r);
    }
    println!("");
}