


use std::fs;

fn same_but_opposite( c1 : char, c2 : char)->bool
{
    let mut s1 = String::new();
    s1.push(c1);
    let mut s2 = String::new();
    s2.push(c2);

    let sbo = (s1.to_uppercase() == s2 && s2.to_lowercase() == s1) || 
              (s1.to_lowercase() == s2 && s2.to_uppercase() == s1);

    sbo
}

fn same_but_opposite_u8( c1 : u8, c2 : u8)->bool
{
        //asssume c1 and c2 are a-z, A-Z
    let sbo = ( c1 + 32 == c2) || (c1 - 32 == c2);
    sbo
}

pub fn answer1()
{
    let mut contents = fs::read_to_string("data/day5.txt").unwrap();

    let mut replacements_made = true;
    while replacements_made {
        replacements_made = false;
        let mut string_to_remove = String::new();

{
        //run through string
        //if we find a suitable pair replace them and start again
        let mut char_it = contents.chars();
        let mut l1 = char_it.next().unwrap();
        let mut l2 = char_it.next();
        while l2 != None {
            
            {
            let l2 = l2.unwrap();
            if same_but_opposite( l1 , l2) 
            {
               string_to_remove.push(l1);
               string_to_remove.push(l2); 
                 replacements_made = true;
                 break;
            }

            l1 = l2;
            }
            l2 = char_it.next();
        }
}
        if( replacements_made )
        {
            contents = contents.replacen(string_to_remove.as_str(), "", 1);
        }
    }
   

    println!("The answer is  {}", contents);
}


struct CharNode
{
    prev: usize,
    next: usize,
}

struct CharList
{
    head: usize,
    nodes: Vec<CharNode>,
    nullptr: usize,
}


fn calc_list_length( list: &CharList)->usize
{
    let mut current = list.head;
    let mut count = 0;
    while current != list.nullptr {
        count += 1;
        current = list.nodes[current].next;
    }
    count
}

fn remove_letter( letter: u8, mut list: CharList, bytes: &[u8])-> CharList
{
    let mut current = list.head;
  

    while current != list.nullptr {

      
        let l1 = bytes[current];

        if l1 == letter || l1 == letter+32 {

        
            let prev = list.nodes[current].prev;
            let next = list.nodes[current].next;
           
            if prev == list.nullptr 
            {
                list.head = next;
            }
            else
            {
                list.nodes[prev].next = next;
            }

            if next != list.nullptr
            {     
                list.nodes[next].prev = prev;
            }
         
            current = next;
   
        }
        else
        {
            //normal iteration
            current = list.nodes[current].next;
        }
    }

    list
}

fn react_list(mut list: CharList, bytes: &[u8])-> CharList
{
    let mut current = list.nodes[list.head].next;
    let mut prev = list.head;

    while current != list.nullptr {

        let l1 = bytes[prev];
        let l2 = bytes[current];

        if same_but_opposite_u8( l1, l2) {

           // (prev, current, head) = cut_pair(prev,current, head);

           prev = list.nodes[prev].prev;
           current = list.nodes[current].next;

          
            if current == list.nullptr {
                //reached the end
                if prev == list.nullptr {
                    list.head = list.nullptr; //list is empty
                }
                else
                {
                    list.nodes[prev].next = list.nullptr;
                }

            }
            else
            {
                if prev == list.nullptr { //gone  off the front

                    list.nodes[current].prev = prev;
                    list.head = current;
                    prev = current;
                    current = list.nodes[current].next;
                }
                else
                {
                    list.nodes[prev].next = current;
                    list.nodes[current].prev = prev;
                }
            }
           
        }
        else
        {

        //normal iteration
        prev = current;
        current = list.nodes[current].next;
        }
    }

    list
}
pub fn answer2()
{
    let contents = fs::read_to_string("data/day5.txt").unwrap();
    let bytes = contents.as_bytes();
    let contents_size = contents.len();
    let nullptr = contents_size;//contents size acts as nullptr

    let mut min_count : usize= contents_size;

    for letter in 65..=90 as u8
    {

        //set up the list
        let mut nodes = Vec::new();
        let contents_size = contents.len();
        nodes.reserve(contents_size);

        let mut head : usize= 0;
    let mut prev: usize = nullptr; 
  
    
    for i in 0..contents_size {
     
        nodes.push( CharNode{prev, next: i+1} );

        prev = i;
    }
    let mut list = CharList{head, nodes, nullptr};

        //remove the letter from the list
        list = remove_letter(letter, list, &bytes);

        //decay
        list = react_list(list, &bytes);

        //get count
        let count = calc_list_length( &list);


        println!("The answer for {} is  {}", letter, count);
        //store minimum
        if count < min_count {
            min_count = count;
        }
    }
   

   
    println!("The answer is  {}", min_count);
    


    //iterate through the list
    
    //get prev and current chars
    //if the chars are same but opposite then remove by
        // current moves to next 
        // prev moves to prev
        // prev.next = current
        // current.prev = prev
        // handle head

    //starting at the head - count the length of the list
    //let count = calc_list_length( &list);
   
   

    //println!("The answer is  {}", count);
}