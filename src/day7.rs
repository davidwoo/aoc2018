
use input;
use std::collections::HashSet;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::collections::BinaryHeap;
use std::cmp::Ordering;

#[derive(Copy, Clone, Eq, PartialEq)]
struct Node
{
    letter: u8,
}

// The priority queue depends on `Ord`.
// Explicitly implement the trait so the queue becomes a min-heap
// instead of a max-heap.
impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other.letter.cmp(&self.letter)
         
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn parse_lines(lines: &Vec<String> )->HashMap<u8, HashSet<u8> >
{
    let mut requirements :HashMap<u8,HashSet<u8>> = HashMap::new();
    

    for line in lines{
        let words : Vec<&str> = line.split_whitespace().collect();

        let l1 : u8 = words[1].as_bytes()[0] ;
        let l2 : u8 = words[7].as_bytes()[0] ;

        requirements.entry(l1).or_insert( HashSet::new() );
        let mut set2 = requirements.entry(l2).or_insert( HashSet::new() );

        set2.insert(l1);
    }
    requirements
}

struct Store
{
    queue : BinaryHeap<Node>,
   // queued : HashSet<usize>,
    requirements: HashMap<u8,HashSet<u8>>
}

fn update_queue( mut store: Store)->Store
{
    let mut queued = HashSet::new();
    //get letters with no requirements
    for (k,v) in store.requirements.iter(){

        if v.is_empty()
        {
            queued.insert(*k);
        }
    }

    //remove the queued items from the requirements of other letters
    for letter in queued.iter() {
          
        store.queue.push(Node{letter:*letter});
        //store.queued.insert(*letter);
        store.requirements.remove(letter);
       
    }

    store
}



pub fn answer1()
{
    let lines = input::read_lines("data/day7.txt");

    //parse lines -> vec<Sets<u8>>
    let requirements = parse_lines(&lines);

    //find letters that are not in any set -> store
    let mut store = Store{
           queue : BinaryHeap::new(),
           // queued : HashSet::new(),
        requirements: requirements,    
    };

    store = update_queue(store);
    let mut path = Vec::new();

    //while store not empty
    while let Some( Node{letter} ) = store.queue.pop()
    {
       // store.queued.remove(&letter);

        //choose first -> path
        path.push( letter );

        for (_,v) in store.requirements.iter_mut(){
            v.remove(&letter);
        }

        //get newly freed letters and add to store queue
        store = update_queue( store );
 
    }
    //print path
    let mut path_string = String::new();
    for l in path.iter(){
        path_string.push( char::from( *l ));
    }
    println!("The path is {}", path_string);
}


struct Worker
{
    letter : u8,
    time_left : u8,
}

impl Worker{

    fn new()->Worker
    {
        Worker{ letter: 65, time_left: 0}
    }
}

struct Workers{
    free: Vec<Worker>,
    active: Vec<Worker>,
    seconds_worked: u32
}

fn process_workers(mut workers: Workers)->Workers
{
    let mut time = 255;
    for worker in workers.active.iter(){
        if worker.time_left < time{
            time = worker.time_left;
        }
    }
    for worker in workers.active.iter_mut(){
        worker.time_left -= time;
    }

    workers.seconds_worked += time as u32;
    workers
}

pub fn answer2()
{
    let lines = input::read_lines("data/day7.txt");

    //parse lines -> vec<Sets<u8>>
    let requirements = parse_lines(&lines);

    //find letters that are not in any set -> store
    let mut store = Store{
           queue : BinaryHeap::new(),
           // queued : HashSet::new(),
        requirements: requirements,    
    };

    store = update_queue(store);
    let mut path = Vec::new();

    let mut workers = Workers{
        free: Vec::new(),
        active: Vec::new(),
        seconds_worked: 0,
    };
   
    let num_workers = 5;
    for _ in 0..num_workers{
        workers.free.push( Worker::new() );
    }
    //while the queue is not empty
    while !store.queue.is_empty() || !workers.active.is_empty(){

        //while there are workers free 
        //pop the queue
        //give to a worker
        while let Some(mut worker) = workers.free.pop()
        {
            match store.queue.pop() 
            {
                Some(node)=>{
                    worker.letter = node.letter;
                    worker.time_left = node.letter - 4;
                    workers.active.push(worker);
                },
                None=>{
                    workers.free.push(worker);
                    break;
                }
            }
        } 


        //process workers until at least one finishes
        workers = process_workers(workers);
        
        let mut finished_work = Vec::new();
        for (i,worker) in workers.active.iter().enumerate()
        {
            if worker.time_left == 0 {
                finished_work.push(i);

                //get letter of each finished worker
                let letter = worker.letter;
        
                //add to path - not needed
                 path.push( letter );
        //process requirements
        //update queue
                for (_,v) in store.requirements.iter_mut(){
                    v.remove(&letter);
                }

                //get newly freed letters and add to store queue
                store = update_queue( store );
            }
      
        }

        for i in finished_work{
            workers.free.push( workers.active.remove(i));
        }

    }


    //print path
    
    let mut path_string = String::new();
    for l in path.iter(){
        path_string.push( char::from( *l ));
    }
    
    println!("The time rquired is {} seconds", workers.seconds_worked);
}