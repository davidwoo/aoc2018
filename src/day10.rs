
use input;
use std::collections::HashMap;

struct Data{
    count : usize,
    y_coords: Vec<i32>,
}

impl Data
{
    fn new() ->Data
    {
        Data{ count: 0, y_coords: Vec::new() }
    }
}

fn is_chaotic( points: &Vec<(i32,i32)>)->bool
{
     let mut line_count = 0;
    //let mut y_counts = HashMap::new();
    let mut x_counts = HashMap::new();

    for point in points{
        let mut data = x_counts.entry(point.0).or_insert(Data::new());

        if !data.y_coords.contains(&point.1) {

        
        data.count += 1;
        data.y_coords.push(point.1);
        }
        //*(y_counts.entry(point.1).or_insert(0)) += 1;
    }

    let min_length = 6;
    for  d in x_counts.values_mut(){

        if d.y_coords.len() >= min_length {
        d.y_coords.sort();

        let ys = &d.y_coords;
        let mut max_run = 0;
        let mut run = 0;
        for i in 1..ys.len(){
            if ys[i] -1 == ys[i-1] {
                run +=1;
            }
            else
            {
                if run > max_run {
                    max_run = run;
                }
                run = 0;
            }
        }

        if run > max_run {
             max_run = run;
        }
        if max_run >= min_length {
            line_count += 1;
        }
        }
    }
    //x_counts.iter().map(|d|d.1.y_coords.sort());
/*
    //find the max count
    let mut max_x = 0;
    let mut max_x_count = 0;
    let mut max_x_coords = Vec::new();
    for (k,val) in &x_counts {
        if val.count > max_x {
            max_x = val.count;
            max_x_count = 1;
            max_x_coords.clear();
            max_x_coords.push(k);
        }
        else if val.count == max_x {
            max_x_count += 1;
            max_x_coords.push(k);
        }
    }

    let mut line_count = 0;
    for x in max_x_coords {
       
        let d = &x_counts[x].y_coords;
        let mut max_run = 0;
        let mut run = 0;
        for i in 1..d.len(){
            if d[i] -1 == d[i-1] {
                run +=1;
            }
            else
            {
                if run > max_run {
                    max_run = run;
                }
                run = 0;
            }
        }

        if run > max_run {
             max_run = run;
        }
        if max_run >= 3 {
            line_count += 1;
        }

    }
*/
    line_count < 6 
/*
    let mut max_y = 0;
    let mut max_y_count = 0;
     for (_,val) in y_counts {
        if val > max_y {
            max_y = val;
        }
    }

    
    max_x < 8 || max_y < 5
    */
}

fn print_points( points: &Vec<(i32,i32)>)
{
    //find the min and max x,y
    let mut min_x = i32::max_value();
    let mut min_y = i32::max_value();
    let mut max_y = i32::min_value();
    let mut max_x = i32::min_value();
    for point in points {
       if point.0 < min_x {
           min_x = point.0;
       } 
       if point.0 > max_x {
           max_x = point.0;
       } 
       if point.1 < min_y {
           min_y = point.1;
       } 
       if point.1 > max_y {
           max_y = point.1;
       } 
    }
    let x_range = max_x - min_x + 3;
    let y_range = max_y - min_y + 3;

    let row = vec!['.';x_range as usize];
   
    let mut rows = vec![row;y_range as usize];
 
    for point in points {
        rows[(1 + point.1 - min_y) as usize][ (1 + point.0 - min_x) as usize] = '*';
    }
    //take 1 of min 
    //add 1 to max
    //create number of rows 
    //fill in the points
    for row in rows {
        let row_str : String = row.into_iter().collect();
        println!("{}", row_str);
    }
}

fn parse_input( lines: Vec<String>)->(Vec<(i32,i32)> , Vec<(i32,i32)>)
{

    let mut points = Vec::new();
    let mut vels = Vec::new();

    for line in lines {

        let clean_line = line.replace("position=<", "").replace("velocity=<","").replace(",","").replace(">","");
        let nums :Vec<&str> = clean_line.split_whitespace().collect();
      
        let px = nums[0].parse::<i32>().unwrap();
        let py = nums[1].parse::<i32>().unwrap();
        let vx = nums[2].parse::<i32>().unwrap();
        let vy = nums[3].parse::<i32>().unwrap();
        points.push( (px,py));
        vels.push( (vx,vy) );
    } 

    (points, vels)
}

pub fn answer1()
{

    let lines = input::read_lines("data/day10.txt");


    let (mut points,vels) = parse_input(lines);


    //in each step transform the points by adding the velocities
    let mut counter = 0;
    while counter < 10003 || is_chaotic(&points)  {

        let count = points.len();
        for i in 0..count 
        {
            points[i].0 += vels[i].0;
            points[i].1 += vels[i].1;     
        }

        
        counter +=1 ;
          
    }
/*
  while counter < 10100 || is_chaotic(&points)  {

       println!("counter = {}", counter);
       print_points(&points);
       println!("");


        let count = points.len();
        for i in 0..count 
        {
            points[i].0 += vels[i].0;
            points[i].1 += vels[i].1;     
        }

        
        counter +=1 ;
          
    }
     */

    //print out what we have
    print_points(&points);
    println!("it took this long {}", counter);
}