
use input;
use std::i32;
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::VecDeque;
use std::collections::HashMap;
use std::collections::hash_map::Entry;



enum Object{
    Elf(Rc<RefCell<Agent>>),
    Goblin(Rc<RefCell<Agent>>),
    Rock,
    Empty,
}

#[derive(PartialEq,Clone,Copy)]
enum AgentType
{
    Elf,
    Goblin
}

struct Agent{
    pos: (usize, usize),
    hit_points: i32,
    attack: i32,
    cat: AgentType,
    is_dead: bool,
}

impl Agent{
    fn new( pos: (usize,usize) , cat:AgentType, attack:i32)->Agent{
        Agent{
            pos,
            hit_points:200,
            attack,
            cat,
            is_dead: false,
        }
    }

    fn can_move(&self, grid: &Vec<Vec<Object>>)->bool {
            
        let row_count = grid.len();
        let col_count = grid[0].len();

        let mut empty_spaces = 0;
        //look up
        if self.pos.0 > 0 {
            let up_row = self.pos.0 -1;
            let up_col = self.pos.1;

            match (&grid[up_row][up_col], self.cat) { 
                (Object::Goblin(_),AgentType::Elf) =>{
                   return false; 
                },
                (Object::Elf(_),AgentType::Goblin) =>{
                   return false;
                },
                (Object::Empty,_)=>{empty_spaces+=1;},
                _=>(),
            }
        }
        //look left 
        if self.pos.1 > 0 {
            let up_row = self.pos.0;
            let up_col = self.pos.1-1;

            match (&grid[up_row][up_col], self.cat) { 
                   (Object::Goblin(_),AgentType::Elf) =>{
                   return false; 
                },
                (Object::Elf(_),AgentType::Goblin) =>{
                   return false;
                },
                (Object::Empty,_)=>{empty_spaces+=1;},
                _=>(),
                
            }
        }
        //look right
         if self.pos.1 < col_count-1 {
            let up_row = self.pos.0;
            let up_col = self.pos.1+1;

            match (&grid[up_row][up_col], self.cat) { 
                  (Object::Goblin(_),AgentType::Elf) =>{
                   return false; 
                },
                (Object::Elf(_),AgentType::Goblin) =>{
                   return false;
                },
                (Object::Empty,_)=>{empty_spaces+=1;},
                _=>(),
            }
        }
        //look down
        if self.pos.0 < row_count-1 {
            let up_row = self.pos.0+1;
            let up_col = self.pos.1;

            match (&grid[up_row][up_col], self.cat) { 
                (Object::Goblin(_),AgentType::Elf) =>{
                   return false; 
                },
                (Object::Elf(_),AgentType::Goblin) =>{
                   return false;
                },
                (Object::Empty,_)=>{empty_spaces+=1;}, 
                _=>(),
            }
        }

        empty_spaces > 0
    }

    fn take_step(&mut self,  grid: &mut Vec<Vec<Object>> )
    {

     
//get available moves ->move list
        let mut frontier = VecDeque::new();
        frontier.push_back((self.pos,0));

        let mut came_from = HashMap::new();
        came_from.insert( self.pos, None);

   
        let mut end_points = Vec::new();
        let mut end_distance = i32::max_value();
       // let mut end_point = None;

         while !frontier.is_empty() {
            let (current,distance) = frontier.pop_front().unwrap();

            //if current is next to an enemy break
            if next_to_enemy(grid, current, self.cat){
                if distance > end_distance
                {
                    break;
                }
                else if distance == end_distance {
  
                end_points.push(current);
              
                continue;
                }
                else
                {
                    end_distance = distance;
                    end_points.clear();
                    end_points.push(current);
                }
            }

            let empty_spaces = get_empty_spaces(grid, current);
            for n in  empty_spaces{
                
                match came_from.entry(n) {
                    Entry::Occupied(_)=>(),
                    Entry::Vacant(v)=>{
                        v.insert(Some(current));
                        frontier.push_back((n,distance+1));
                    },
                }
            }
        }

        if !end_points.is_empty() 
        {
            end_points.sort();
            let pos = end_points[0];
          
                //back track to get path
                //take a step along the path
                let mut path = Vec::new();
                path.push(pos);
                let mut v = came_from[&pos];
                while match v {
                    Some(n)=>{
                        path.push(n);
                        v = came_from[&n];
                        true
                    }
                    
                     None=>false,
                }
                {
                } 

                let step = path[path.len()-2];
                grid[step.0][step.1] = match &grid[self.pos.0][self.pos.1]
                {
                   Object::Elf(a)=>{Object::Elf(a.clone())},
                   Object::Goblin(a)=>{Object::Goblin(a.clone())},
                   Object::Rock=>Object::Rock,
                   Object::Empty=>Object::Empty,
                };
                grid[self.pos.0][self.pos.1]=Object::Empty;
                self.pos = step;
             //   println!("Found path");
            
         
        }

    }

    //attack target with fewest HP and first in reading order
    fn choose_adjacent_enemy(&self, grid: &Vec<Vec<Object>>)->Option<Rc<RefCell<Agent>>>
    {
      
        let mut min_hit_points = i32::max_value();

        let mut result = None;
        let row_count = grid.len();
        let col_count = grid[0].len();

        if self.cat == AgentType::Elf{

        //look up
        if self.pos.0 > 0 {
            let up_row = self.pos.0 -1;
            let up_col = self.pos.1;

            match grid[up_row][up_col] { 
                Object::Goblin( ref agent) =>{
                    result = test_agent( agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }
        //look left 
        if self.pos.1 > 0 {
            let up_row = self.pos.0;
            let up_col = self.pos.1-1;

            match grid[up_row][up_col] { 
                Object::Goblin( ref agent)=>{
                    result = test_agent(agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }
        //look right
         if self.pos.1 < col_count-1 {
            let up_row = self.pos.0;
            let up_col = self.pos.1+1;

            match grid[up_row][up_col] { 
                Object::Goblin( ref agent)=>{
                    result = test_agent(agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }
        //look down
        if self.pos.0 < row_count-1 {
            let up_row = self.pos.0+1;
            let up_col = self.pos.1;

            match grid[up_row][up_col] { 
                Object::Goblin(ref agent)=>{
                    result = test_agent(agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }

        }
        else
        {
               //look up
        if self.pos.0 > 0 {
            let up_row = self.pos.0 -1;
            let up_col = self.pos.1;

            match grid[up_row][up_col] { 
                Object::Elf( ref agent) =>{
                    result = test_agent(agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }

  
        //look left 
        if self.pos.1 > 0 {
            let up_row = self.pos.0;
            let up_col = self.pos.1-1;

            match grid[up_row][up_col] { 
                Object::Elf(ref agent)=>{
                    result = test_agent(agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }
        //look right
         if self.pos.1 < col_count-1 {
            let up_row = self.pos.0;
            let up_col = self.pos.1+1;

            match grid[up_row][up_col] { 
                Object::Elf( ref agent)=>{
                    result = test_agent(agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }
        //look down
        if self.pos.0 < row_count-1 {
            let up_row = self.pos.0+1;
            let up_col = self.pos.1;

            match grid[up_row][up_col] { 
                Object::Elf(ref agent)=>{
                    result = test_agent(agent, &mut min_hit_points, result); 
                },
                _=>(),
            }
        }
     
        }
        result
    }

    fn take_damage(&mut self, damage: i32,  grid:&mut Vec<Vec<Object>>, elf_count:&mut i32, goblin_count:&mut i32)
    {
        self.hit_points -= damage;
        if self.hit_points <= 0 {
            self.is_dead = true;
            grid[self.pos.0][self.pos.1] = Object::Empty;

            match self.cat{
                AgentType::Elf=>{*elf_count-=1;},
                AgentType::Goblin=>{*goblin_count-=1;},
            }
        }
    }
}

fn next_to_enemy(grid:&Vec<Vec<Object>>, pos:(usize,usize), agent_type:AgentType)->bool{
       let row_count = grid.len();
        let col_count = grid[0].len();

     
        //look up
        if pos.0 > 0 {
            let up_row = pos.0 -1;
            let up_col =pos.1;

            match (&grid[up_row][up_col], agent_type) { 
                (Object::Goblin(_),AgentType::Elf) =>{
                   return true; 
                }, 
                (Object::Elf(_),AgentType::Goblin) =>{
                   return true;
                },
              
                _=>(),
            }
        }
        //look left 
        if pos.1 > 0 {
            let up_row = pos.0;
            let up_col = pos.1-1;

            match (&grid[up_row][up_col], agent_type) { 
                   (Object::Goblin(_),AgentType::Elf) =>{
                   return true; 
                },
                (Object::Elf(_),AgentType::Goblin) =>{
                   return true;
                },
              
                _=>(),
                
            }
        }
        //look right
         if pos.1 < col_count-1 {
            let up_row = pos.0;
            let up_col = pos.1+1;

            match (&grid[up_row][up_col], agent_type) { 
                  (Object::Goblin(_),AgentType::Elf) =>{
                   return true; 
                },
                (Object::Elf(_),AgentType::Goblin) =>{
                   return true;
                },
             
                _=>(),
            }
        }
        //look down
        if pos.0 < row_count-1 {
            let up_row = pos.0+1;
            let up_col = pos.1;

            match (&grid[up_row][up_col], agent_type) { 
                (Object::Goblin(_),AgentType::Elf) =>{
                   return true; 
                },
                (Object::Elf(_),AgentType::Goblin) =>{
                   return true;
                },
               
                _=>(),
            }
        }

        false
}

fn get_empty_spaces(grid:&Vec<Vec<Object>>, current:(usize,usize) )->Vec<(usize,usize)>
{
    let mut result = Vec::new();

    let row_count = grid.len();
    let col_count = grid[0].len();

      //look up
        if current.0 > 0 {
            let up_row = current.0 -1;
            let up_col = current.1;

            match &grid[up_row][up_col] { 
                Object::Empty =>{
                    result.push( (up_row,up_col));
                },
                _=>(),
            }
        }

      
        //look left 
        if current.1 > 0 {
            let up_row = current.0;
            let up_col = current.1-1;

            match &grid[up_row][up_col] { 
                Object::Empty=>{
                       result.push( (up_row,up_col));
                },
                _=>(),
            }
        }
        
        //look right
         if current.1 < col_count-1 {
            let up_row = current.0;
            let up_col = current.1+1;

            match &grid[up_row][up_col] { 
                Object::Empty=>{
                        result.push( (up_row,up_col)); 
                },
                _=>(),
            }
        }
        //look down
        if current.0 < row_count-1 {
            let up_row = current.0+1;
            let up_col = current.1;

            match &grid[up_row][up_col] { 
                 Object::Empty=>{
                       result.push( (up_row,up_col) );
                },
                _=>(),
            }
        }

    result
}

fn test_agent(agent: &Rc<RefCell<Agent>>, min_hit_points:&mut i32, mut result: Option<Rc<RefCell<Agent>>>)->Option<Rc<RefCell<Agent>>>
{
    let agent_health = agent.borrow().hit_points;
    if agent_health < *min_hit_points {
        *min_hit_points = agent_health;
        result = Some((*agent).clone());
    }
    
    result
}

struct Game
{
    grid: Vec<Vec<Object>>,
    agents: Vec<Rc<RefCell<Agent>>>,
    goblin_count: i32,
    elf_count: i32,
    round_count: i32,
}

impl Game{
    fn new(lines: &Vec<String>, elf_power:i32)->Game
    {

        let mut agents = Vec::new();
    let mut grid = Vec::new();
    let mut elf_count = 0;
    let mut goblin_count = 0;

    for (y,line) in lines.iter().enumerate(){

        let mut row = Vec::new();
        for (x,b) in line.chars().enumerate() {

           
            match b {
                '#'=>{row.push(Object::Rock);},
                'G'=>{
                    let bob = Rc::new( RefCell::new( Agent::new((y,x), AgentType::Goblin,3 )));
                    row.push(Object::Goblin(bob.clone()));
                    agents.push( bob) ;
                    goblin_count += 1;
                },
                'E'=>{
                    let elf = Rc::new( RefCell::new( Agent::new((y,x), AgentType::Elf, elf_power)));
                    row.push(Object::Elf(elf.clone()));
                    agents.push( elf );
                    elf_count += 1;
                },
                '.'=>{row.push(Object::Empty);},
                _=>(),
            }
        }
        grid.push(row);
    }

        Game{
            elf_count,
            goblin_count,
            round_count:0,
            agents,
            grid,
        }
    }
}

fn print_grid(grid: &Vec<Vec<Object>>)
{
    for row in grid {
        let mut row_string = String::new();
        let mut agents = Vec::new();
        for obj in row {
            row_string.push( 
                match obj {
                    Object::Elf(a)=>{agents.push(a.clone()); 'E'},
                    Object::Goblin(a)=>{agents.push(a.clone());'G'},
                    Object::Rock=>'#',
                    Object::Empty=>'.',
                }
            ); 
        }
        for a in agents{
            let bob = format!(" {}({})", if a.borrow().cat == AgentType::Elf{'E'}else{'G'}, a.borrow().hit_points);
            row_string.push_str(&bob);
        }
        println!("{}", row_string);
    }
    println!("");
}

pub fn answer1()
{
    //read in the lines and parse
    let lines = input::read_lines("data/day15.txt");

    let mut game = Game::new(&lines,3);
    


    'game_over: 
    while game.elf_count > 0 && game.goblin_count > 0 {

        //sort agents
        game.agents.sort_by(|a1,a2| a1.borrow().pos.cmp(&a2.borrow().pos) );
        
        //for each agent
        for agent in game.agents.iter()
        {

            let mut agent = agent.borrow_mut();
            //if not dead
            if !agent.is_dead {
                
                //if no targets 
                if game.elf_count == 0 || game.goblin_count == 0 {
                    break 'game_over;
                }
                    

                //if not next to a target
                if agent.can_move(&game.grid) {


                    agent.take_step(&mut game.grid);
                    //move towards a target
                    //update the grid
                    //update the agent's coordinates
                }
                

                //if next to a target
                match agent.choose_adjacent_enemy( &game.grid) {

                    Some(enemy)=>{
                        let mut enemy = enemy.borrow_mut();

                        enemy.take_damage(agent.attack, &mut game.grid , &mut game.elf_count, &mut game.goblin_count);
                    },
                    //attack target with fewest HP and first in reading order
                    //update agent hit points
                    //update agent state
                    //update grid maybe
                    None=>(),
                }
            }
        }
        game.round_count += 1;


        //remove dead agents
        game.agents.retain(|a| !a.borrow().is_dead );

        println!("AFter round {} :", game.round_count);
        print_grid(&game.grid);
    }

    println!("AFter round {} :", game.round_count);
    print_grid(&game.grid);

    game.agents.retain(|a| !a.borrow().is_dead );
    let total_hit_points = game.agents.iter().fold(0,|acc,a| acc + a.borrow().hit_points);
    let score = total_hit_points * game.round_count;
    println!("The final score is {}", score);
    
}

pub fn answer2()
{
     //read in the lines and parse
    let lines = input::read_lines("data/day15.txt");


    for elf_power in 4.. {
    let mut game = Game::new(&lines,elf_power);
    
    let start_elf_count = game.elf_count;

    'game_over: 
    while game.elf_count == start_elf_count && game.goblin_count > 0 {

        //sort agents
        game.agents.sort_by(|a1,a2| a1.borrow().pos.cmp(&a2.borrow().pos) );
        
        //for each agent
        for agent in game.agents.iter()
        {

            let mut agent = agent.borrow_mut();
            //if not dead
            if !agent.is_dead {
                
                //if no targets 
                if game.elf_count < start_elf_count || game.goblin_count == 0 {
                    break 'game_over;
                }
                    

                //if not next to a target
                if agent.can_move(&game.grid) {
                    agent.take_step(&mut game.grid);
                }
                

                //if next to a target
                match agent.choose_adjacent_enemy( &game.grid) {

                    Some(enemy)=>{
                        let mut enemy = enemy.borrow_mut();

                        enemy.take_damage(agent.attack, &mut game.grid , &mut game.elf_count, &mut game.goblin_count);
                    },
                   
                    None=>(),
                }
            }
        }
        game.round_count += 1;


        //remove dead agents
        game.agents.retain(|a| !a.borrow().is_dead );

        println!("AFter round {} :", game.round_count);
        print_grid(&game.grid);
    }

    if start_elf_count == game.elf_count {

    println!("AFter round {} :", game.round_count);
    print_grid(&game.grid);

    game.agents.retain(|a| !a.borrow().is_dead );
    let total_hit_points = game.agents.iter().fold(0,|acc,a| acc + a.borrow().hit_points);
    let score = total_hit_points * game.round_count;
    println!("The final score is {}", score);
    break;
    }
    else
    {
        println!("elves died with attack power = {}", elf_power);
    }
}
}