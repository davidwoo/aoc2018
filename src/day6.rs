
use input;
use std::i32;
use std::collections::HashSet;

struct GridPos
{
    index:usize,
    distance:i32,
    xpos:i32,
    ypos:i32,
}
pub fn answer1()
{

    let mut coords = Vec::new();
    let mut min_x = i32::max_value();
    let mut max_x = i32::min_value();
    let mut min_y = i32::max_value();
    let mut max_y = i32::min_value();
   let lines = input::read_lines("data/day6.txt");
        for  line in lines {
            let pair:Vec<&str> = line.split(", ").collect();
            let x =  pair[0].parse::<i32>().unwrap();
            let y =  pair[1].parse::<i32>().unwrap();

            if x < min_x {
                min_x = x;
            }
            if x > max_x {
                max_x = x
            }
            if y < min_y {
                min_y = y;
            }
            if y > max_y {
                max_y = y;
            }

            coords.push( (x,y) );
        } 


    //find min x,y and max xy
    let num_coords = coords.len();
    let width = max_x - min_x + 1;
    let height = max_y - min_y + 1;
    let size = width * height;
    let mut grid = Vec::new();
    grid.reserve(size as usize);
 


    //for each position in grid mark with index of nearest coord and distance to it
    for i in 0..size {

        let xpos = i % width + min_x;
        let ypos = i / width + min_y;

        //find distance to each coord
        let mut min_distance = i32::max_value();
        let mut min_index = usize::max_value();
        for (c_index , coord) in coords.iter().enumerate() {

            let (cx,cy) = coord;
            let distance = (cx-xpos).abs() + (cy-ypos).abs();
            if distance == min_distance {
                min_index = usize::max_value();
            }
            else if distance < min_distance {
                min_index = c_index;
                min_distance = distance;
            }
        }

        grid.push( GridPos{index:min_index, distance:min_distance, xpos, ypos});
    }

    let mut counts = vec![0; num_coords];
    let mut invalids = HashSet::new();
    for grid_pos in grid {
        if grid_pos.index != usize::max_value() {
            counts[grid_pos.index] += 1;

            if grid_pos.xpos == min_x {
                invalids.insert( grid_pos.index );
            }
            else if grid_pos.xpos == max_x {
                invalids.insert( grid_pos.index );
            }

            if grid_pos.ypos == min_y {
                invalids.insert( grid_pos.index );
            }
            else if grid_pos.ypos == max_y {
                invalids.insert( grid_pos.index );
            }
        }
    }
    //reset the invalid
    for index in invalids{
        counts[index] = 0;
    }

    //find the index with the largeest value
    let mut best = i32::min_value();
    let mut best_i:usize = 0;
    for (i,value) in counts.iter().enumerate() {
        if *value > best {
            best = *value;
            best_i = i;
        }
    }

    println!("The largest finite area is {}", best);
}

pub fn answer2()
{
      let mut coords = Vec::new();
    let mut min_x = i32::max_value();
    let mut max_x = i32::min_value();
    let mut min_y = i32::max_value();
    let mut max_y = i32::min_value();
   let lines = input::read_lines("data/day6.txt");
        for  line in lines {
            let pair:Vec<&str> = line.split(", ").collect();
            let x =  pair[0].parse::<i32>().unwrap();
            let y =  pair[1].parse::<i32>().unwrap();

            if x < min_x {
                min_x = x;
            }
            if x > max_x {
                max_x = x
            }
            if y < min_y {
                min_y = y;
            }
            if y > max_y {
                max_y = y;
            }

            coords.push( (x,y) );
        } 


    //find min x,y and max xy
    let num_coords = coords.len();
    let width = max_x - min_x + 1;
    let height = max_y - min_y + 1;
    let size = width * height;

    let grid_xstart = min_x - 200;
    let grid_xend = max_x + 200;
    let grid_ystart = min_y - 200;
    let grid_yend = max_x + 200;

    let mut safe_count = 0;
         for y in grid_ystart..=grid_yend{
    for x in grid_xstart..=grid_xend{
   

            let mut total_distance = 0;
         for  coord in coords.iter(){

            let (cx,cy) = coord;
            let distance = (cx-x).abs() + (cy-y).abs();
            total_distance += distance;
        }
            if total_distance < 10000
            {
                safe_count += 1;
            }
        }
    }

    println!("The area of safe count is {}", safe_count);
}