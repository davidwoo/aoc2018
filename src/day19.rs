    use crate::input;

    const REGISTER_COUNT:usize = 6;

    type Registers = [usize;REGISTER_COUNT];

    type Instructions = [usize; 3];

    #[derive( PartialEq, Hash, Eq, Clone, Copy)]
    enum OpCode
    {
        Addr(Instructions),
        Addi(Instructions),
        Mulr(Instructions),
        Muli(Instructions),
        Banr(Instructions),
        Bani(Instructions),
        Borr(Instructions),
        Bori(Instructions),
        Setr(Instructions),
        Seti(Instructions),
        Gtir(Instructions),
        Gtri(Instructions),
        Gtrr(Instructions),
        Eqir(Instructions),
        Eqri(Instructions),
        Eqrr(Instructions),
    }

    impl OpCode{

        fn do_op(&self,  mut regs : Registers) ->Registers
        {

            match self {
                OpCode::Addr([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] + regs[ *b ];  
                },
                OpCode::Addi([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] + *b;  
                },
                OpCode::Mulr([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] * regs[ *b ];  
                },
                OpCode::Muli([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] * *b;  
                },
                OpCode::Banr([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] & regs[ *b ];  
                },
                OpCode::Bani([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] & *b;
                },
                OpCode::Borr([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] | regs[ *b ]; 
                },
                OpCode::Bori([a,b,c])=>{
                    regs[ *c ] = regs[ *a ] | *b;
                },
                OpCode::Setr([a,_,c])=>{
                    regs[ *c ] = regs[ *a ];
                },
                OpCode::Seti([a,_,c])=>{
                    regs[ *c ] = *a;
                },
                OpCode::Gtir([a,b,c])=>{
                    regs[ *c ] = if *a  > regs[ *b ] {1} else {0}; 
                },
                OpCode::Gtri([a,b,c])=>{
                    regs[ *c ] = if regs[ *a ] > *b  {1} else {0}; 
                },
                OpCode::Gtrr([a,b,c])=>{
                    regs[ *c ] = if regs[ *a ]  > regs[ *b ] {1} else {0}; 
                },
                OpCode::Eqir([a,b,c])=>{
                    regs[ *c ] = if *a  == regs[ *b ] {1} else {0}; 
                },
                OpCode::Eqri([a,b,c])=>{
                    regs[ *c ] = if regs[ *a ] == *b  {1} else {0}; 
                },
                OpCode::Eqrr([a,b,c])=>{
                    regs[ *c ] = if regs[ *a ]  == regs[ *b ] {1} else {0};
                },
            }

            regs
        }

    }

 
    fn parse_instructions<'a, T>(tokens: &mut T)->Instructions
    where
    T: Iterator<Item=&'a str>
        {
            [
                tokens.next().unwrap().parse().unwrap(),
                tokens.next().unwrap().parse().unwrap(),
                tokens.next().unwrap().parse().unwrap(),
            ]
    }
    

    struct Program
    {
        ip: usize,
        ip_binding: usize,
        instructions : Vec<OpCode>,
        registers: Registers,
        state: State,
        inst_strings : Vec<String>,
    }

    #[derive(Eq, PartialEq,Copy, Clone)]
    enum State{
        Running,
        Stopped,
    }

    impl Program {

        fn new(lines:Vec<String>)->Program{

            let mut ip_binding = 0;
            let mut instructions = Vec::new();
            let mut inst_strings = Vec::new();

            for line in lines {
                inst_strings.push(line.clone());
                let mut tokens = line.split_whitespace();
                match tokens.next() {
                    Some("#ip") =>{ ip_binding = tokens.next().unwrap().parse().unwrap(); inst_strings.pop();},

                    Some("addr") =>{ instructions.push( OpCode::Addr( parse_instructions(&mut tokens) )); },
                    Some("addi") =>{ instructions.push( OpCode::Addi( parse_instructions(&mut tokens) )); },
                    Some("mulr") =>{ instructions.push( OpCode::Mulr( parse_instructions(&mut tokens) )); },
                    Some("muli") =>{ instructions.push( OpCode::Muli( parse_instructions(&mut tokens) ));  },
                    Some("banr") =>{ instructions.push( OpCode::Banr( parse_instructions(&mut tokens) ));  },
                    Some("bani") =>{ instructions.push( OpCode::Bani( parse_instructions(&mut tokens) ));  },
                    Some("borr") =>{ instructions.push( OpCode::Borr( parse_instructions(&mut tokens) ));  },
                    Some("bori") =>{ instructions.push( OpCode::Bori( parse_instructions(&mut tokens) ));  },
                    Some("setr") =>{ instructions.push( OpCode::Setr( parse_instructions(&mut tokens) ));  },
                    Some("seti") =>{ instructions.push( OpCode::Seti( parse_instructions(&mut tokens) ));  },
                    Some("gtir") =>{ instructions.push( OpCode::Gtir( parse_instructions(&mut tokens) ));  },
                    Some("gtri") =>{ instructions.push( OpCode::Gtri( parse_instructions(&mut tokens) ));  },
                    Some("gtrr") =>{ instructions.push( OpCode::Gtrr( parse_instructions(&mut tokens) ));  },
                    Some("eqir") =>{ instructions.push( OpCode::Eqir( parse_instructions(&mut tokens) ));  },
                    Some("eqri") =>{ instructions.push( OpCode::Eqri( parse_instructions(&mut tokens) ));  },
                    Some("eqrr") =>{ instructions.push( OpCode::Eqrr( parse_instructions(&mut tokens) ));  },

                    _=>{},
                }
            }

            Program{
                ip:0,
                ip_binding,
                instructions,
                registers: [1,0,0,0,0,0],
                state: State::Running,
                inst_strings,
            }
        }

        fn step(&mut self)
        {
            if self.state == State::Running {

                //copy ip to register [ip_binding]
                self.registers[ self.ip_binding ] = self.ip;

                self.print_ip();
                self.print_registers();

                //execute instructions[ip]
                self.print_current_instruction();
                self.registers = self.instructions[ self.ip ].do_op( self.registers );
                
                //copy register[ip_binding]->ip
                self.ip = self.registers[ self.ip_binding ];

                //increment ip
                self.ip += 1;

                self.print_registers();
                println!();


                //check ip validity
                if self.ip >= self.instructions.len() {
                    self.state = State::Stopped;
                } 
            }
        }

        fn is_stopped(&self)->bool
        {
            self.state == State::Stopped
        }

        fn print_ip(&self)
        {
            print!("ip={}", self.ip); 
        }

        fn print_registers(&self)
        {
            print!(" {:?}", self.registers);
        }

        fn print_current_instruction(&self)
        {
            print!(" {}", self.inst_strings[self.ip]);
        }

        fn print(&self)
        {
            self.print_ip();
            self.print_registers();
            println!();
        }
    }

    pub fn answer1()
    {
        let lines = input::read_lines("data/day21.txt");

        let mut program = Program::new(lines);

        //program.print();

        while !program.is_stopped() {
            program.step();

        }

        program.print();
        println!("Hello World");
    }

    pub fn answer2()
    {

        let factors = [
            1, 2, 443, 886, 11909, 23818, 5275687, 10551374,
        ]; 

        let r0:u64  = factors.iter().sum();
        /*
       // let r3 = 974;
       let r3 = 10551374;
        let mut r0 = 0;
        let mut r4 = 1;
        while r4 <= r3 {

            let mut r5 = 1;

            while r5 <= r3 {
                let r1 = r4 * r5;
                if r1 == r3 {
                    r0 += r4;
                }

                r5+=1;
            }

            r4 += 1;
        }
*/
        println!("r0 = {}",r0);
    }