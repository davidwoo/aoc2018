
use crate::input;
use regex::Regex;
use std::cmp;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::hash::Hash;

//battle
//army1 - reindeer
//army2 - infection

//army has a list of groups

#[derive(Debug, Eq, PartialEq, Copy,Clone, Hash)]
enum Side{
    Reindeer,
    Infection,
}

#[derive(Eq, PartialEq,Copy,Clone, Hash)]
enum AttackType
{
    Fire,
    Cold,
    Slashing,
    Radiation,
    Bludgeoning,
}

#[derive(Eq, PartialEq, Hash)]
struct Group{
    initial_count : i32,
    id: usize, 
    side : Side,
    hit_points : i32,
    attack_damage: i32,
    initial_damage: i32,
    attack_type: AttackType,
    initiative: i32,
    weaknesses : Vec<AttackType>,
    immunities : Vec<AttackType>,
}




fn attack_type_from_str(text: &str)->AttackType{
    match text
        {
            "cold" => AttackType::Cold,
            "fire" => AttackType::Fire,
            "bludgeoning" => AttackType::Bludgeoning,
            "radiation" => AttackType::Radiation,
            "slashing" => AttackType::Slashing,
            _ =>
            { 
                println!("The bad text is {}", text);
            panic!()
            },
        }
}

impl Group{

    fn apply_boost(&mut self, boost: i32)
    {
        self.attack_damage = boost + self.initial_damage;
    }

    fn calc_damage(&self, count: i32, target: &Group)->i32 {
        
        if target.immunities.contains( &self.attack_type )
        {
            0
        }
        else if target.weaknesses.contains( &self.attack_type )
        {
            self.effective_power(count) * 2
        }
        else
        {
            self.effective_power(count)
        }
    }

    fn id(&self)->usize
    {
        self.id
    }

  

    fn initiative(&self)->i32{
        self.initiative
    }
  

    fn effective_power(&self, count: i32)->i32
    {
        count * self.attack_damage 
    }

    fn new( description : String, side : Side, id: usize )->Group
    {
        //(?P<attack_type>[fire|cold|radiation|slashing|bludgeoning]) 
        let pattern =  Regex::new( r"^(?P<count>\d+) units each with (?P<hp>\d+) hit points (weak:\((?P<weak>.+)\) )?(immune:\[(?P<immune>.+)\] )?with an attack that does (?P<damage>\d+) (?P<attack_type>fire|cold|radiation|slashing|bludgeoning) damage at initiative (?P<init>\d+)$" ).unwrap();

        let captures = pattern.captures(&description).unwrap();

        let initial_count = captures.name("count").unwrap().as_str().parse().unwrap();
        let hit_points = captures.name("hp").unwrap().as_str().parse().unwrap();
        let attack_damage = captures.name("damage").unwrap().as_str().parse().unwrap();
        let initiative = captures.name("init").unwrap().as_str().parse().unwrap();
        let attack_type = attack_type_from_str( captures.name("attack_type").unwrap().as_str() );
      
       
        let mut weaknesses = Vec::new();
      

        if let Some(list) = captures.name("weak")
        {
           // let pattern =  Regex::new( r"\((weak to );?\) " );
            for sub in list.as_str().split(", ")
            {
                weaknesses.push( attack_type_from_str(sub));
            }
        }

        let mut immunities = Vec::new();
          if let Some(list) = captures.name("immune")
        {
           // let pattern =  Regex::new( r"\((weak to );?\) " );
            let matches = list.as_str();
            for sub in matches.split(", ")
            {
                immunities.push( attack_type_from_str(sub));
            }
        }


        Group{
            initial_count,
            id,
            side,
            hit_points,
            attack_damage,
            initial_damage: attack_damage,
            attack_type,
            initiative,
            weaknesses,
            immunities,
        }
    }
}
//group has
    //side
    //number
    //type
    //hitpoints
    //attack damage
    //attack type
    //initiative
    //list of weaknesses
    //list of strengths

    //effective power = number of units * attack damage

//attack types
/*
    fire,
    cold,
    bludgeoning,
    radiation,
    slashing
*/

//targeting phase
    //sort by effective power then initiative
    //select enemy to which it would deal the most damage then effective power then initiative

    //The damage an attacking group deals to a defending group depends on the attacking group's 
    //attack type and the defending group's immunities and weaknesses. By default, an attacking 
    //group would deal damage equal to its effective power to the defending group. However, if the 
    //defending group is immune to the attacking group's attack type, the defending group instead 
    //takes no damage; if the defending group is weak to the attacking group's attack type, the
    // defending group instead takes double damage.

    //if no damage then no target
    //only one target per attacker
    //can only be targeted once

//attack phase
    //sort by initative

//The defending group only loses whole units from damage; 
//damage is always dealt in such a way that it kills the most units possible,
// and any remaining damage to a unit that does not immediately kill it is ignored. 
//For example, if a defending group contains 10 units with 10 hit points each and receives 75 damage, 
//it loses exactly 7 units and is left with 3 units at full health.  damage % hitpoints

fn add_groups( groups: &mut Vec<Group>, lines: Vec<String>, side: Side)
{
    for line in lines 
    {
        groups.push( Group::new(line, side, groups.len() ) );
    }
}

fn has_two_sides(groups: &Vec<Group>)->bool
{
    
    let counts = groups.iter().fold((0,0), 
    |c,g| match g.side {
        Side::Reindeer => (c.0 +1, c.1),
        Side::Infection => (c.0, c.1 + 1),
    }
    );

    counts.0 > 0 && counts.1 > 0
}


fn attack_ordering(group1: &Group, group2: & Group)->Ordering
{
    group2.initiative().cmp(&group1.initiative())
}



pub fn answer1()
{
    let rein_lines = input::read_lines("data/day24immune.txt");
    let infection_lines = input::read_lines("data/day24infection.txt");

    let mut all = Vec::new();
    add_groups( &mut all, rein_lines, Side::Reindeer);
    println!("Processed immune system");

 
    add_groups( &mut all, infection_lines, Side::Infection);

    println!("processed infection.");

    let mut counts : Vec<i32> = all.iter().map(|g|g.initial_count).collect();

    while has_two_sides(&all)
    {        
        //targeting phase

        //sort groups
        all.sort_by( |g1,g2| match g2.effective_power(counts[g2.id]).cmp( &g1.effective_power(counts[g1.id]) )
        {
            Ordering::Equal => g2.initiative().cmp( &g1.initiative() ),
            x => x,
        });


        //split sides for targeting
        let mut rs: Vec<&Group> = all.iter().filter(|g| g.side == Side::Reindeer).collect();
        let mut is: Vec<&Group> = all.iter().filter(|g| g.side == Side::Infection).collect();
        
        println!("Reindeer");
        for g in &rs
        {
            println!("Group {} contains {} units", g.id(), counts[g.id()]);
        }
        println!("Infection");
        for g in &is
        {
            println!("Group {} contains {} units", g.id(), counts[g.id()]);
        }
        println!();

        let mut target_map = HashMap::new();
        
        for group in &all
        {
            //select the enemy to whom would do the most damage
            let enemies = match group.side {
                Side::Reindeer => &mut is,
                Side::Infection => &mut rs,
            };


         
         
            let group_count = counts[group.id];


              
            let mut choice = None;
            for (i, e) in enemies.iter().enumerate() 
            {
                //calc the damage group can do to e
                let damage = group.calc_damage( group_count, e );

                if damage > 0 {
                    match choice 
                    {
                        Some((t, d, _)) => {
                            if damage > d {
                                choice = Some((e, damage, i));
                            }
                            else if damage == d {
                                let e_pow = e.effective_power(counts[e.id()]);
                                let t_pow = t.effective_power(counts[t.id()]);
                                if  e_pow > t_pow {
                                    choice = Some((e,damage, i));
                                }
                                else if e_pow == t_pow{
                                    if e.initiative() > t. initiative() {
                                        choice = Some((e,damage, i));
                                    }
                                }
                            }
                        },
                        None => choice = Some((e, damage, i)),
                    }
                }
                       
            }
            
/*
           let choice = enemies.iter().enumerate()
           .map(|e| (e.1, group.calc_damage(group_count, e.1), ) )
           .max_by(|e1,e2| {
               match e2.1.cmp(&e1.1){
                   Ordering::Equal =>  match e2.0.effective_power(counts[e2.0.id]).cmp(&e1.0.effective_power(counts[e1.0.id])) 
                   {
                        Ordering::Equal => e2.0.initiative().cmp(&e1.0.initiative()),
                        o => o, 
                   },
                   o => o,
               }
           });*/

           if let Some(thing) = choice {
                //calculate the target which will take most damage
                //and the damage amount
                //store that if can damage anything 
                let target_index = thing.2;
                let pot_damage = thing.1;
                let target_id = thing.0.id();
              
                if pot_damage > 0
                {

                    println!("{:?} Group {} is targeting group {} for pot_damage {} ", group.side, group.id(), target_id, pot_damage);
                //select enemy to which it would deal the most damage then effective power then initiative
                //let target = group;
                   enemies.remove(target_index);
                    target_map.insert( group.id(), target_id );
                }
           }
           
        } 

        println!();
        //attack phase
        //sort groups
        all.sort_by(attack_ordering);
        for group in &all{
            if counts[group.id] > 0 {

               if let Some(target_id) = target_map.get(&group.id())
               {
                   let t = all.iter().find(|g| {g.id() == *target_id} ).unwrap();
                   let damage = group.calc_damage(counts[group.id()], t);
                   let units_killed = cmp::min( counts[*target_id], damage / t.hit_points);
                   println!("{:?} Group {} is attacking group {} for damage {} killing {} units", group.side, group.id(), target_id, damage, units_killed);
                   counts[ *target_id ] -= units_killed; 
               }
                //find the target for the group 
                //attack the targeted group if there is one
                //target->take_damage(group);
                //target.apply_damage();
            }
                   
        }
            
        println!();    
                
        //remove dead groups
        all.retain(|g| counts[g.id] > 0 );
        
    }

    let unit_count = all.iter().fold(0, |v, g| v + counts[g.id()]);
    println!("Remaining units = {}", unit_count);
}


fn has_two_sides2(groups: &[&Group])->bool
{
    
    let counts = groups.iter().fold((0,0), 
    |c,g| match g.side {
        Side::Reindeer => (c.0 +1, c.1),
        Side::Infection => (c.0, c.1 + 1),
    }
    );

    counts.0 > 0 && counts.1 > 0
}
pub fn answer2()
{
    let rein_lines = input::read_lines("data/day24immune.txt");
    let infection_lines = input::read_lines("data/day24infection.txt");

    let mut groups = Vec::new();
    add_groups( &mut groups, rein_lines, Side::Reindeer);
    println!("Processed immune system");

 
    add_groups( &mut groups, infection_lines, Side::Infection);

    println!("processed infection.");

    let mut min_boost = 0;
    let mut boost_step = 1 << 15;
    let mut boost = 0;//boost_step;
    let mut was_draw = false;

    loop{
        for g in groups.iter_mut().filter(|g| g.side == Side::Reindeer )
        {
            g.apply_boost(boost);
        }

    let mut all : Vec<&Group> = groups.iter().collect();
    let mut counts : Vec<i32> = all.iter().map(|g|g.initial_count).collect();

    was_draw = false;
    
    while has_two_sides2(&all)
    {        
        //targeting phase

        //sort groups
        all.sort_by( |g1,g2| match g2.effective_power(counts[g2.id]).cmp( &g1.effective_power(counts[g1.id]) )
        {
            Ordering::Equal => g2.initiative().cmp( &g1.initiative() ),
            x => x,
        });


        //split sides for targeting
        let mut rs: Vec<&Group> = all.iter().filter(|g| g.side == Side::Reindeer).map(|g|*g).collect();
        let mut is: Vec<&Group> = all.iter().filter(|g| g.side == Side::Infection).map(|g|*g).collect();

      /*  
        println!("Reindeer");
        for g in &rs
        {
            println!("Group {} contains {} units", g.id(), counts[g.id()]);
        }
        println!("Infection");
        for g in &is
        {
            println!("Group {} contains {} units", g.id(), counts[g.id()]);
        }
        println!();
        */

        let mut target_map = HashMap::new();
        
        for group in &all
        {
            //select the enemy to whom would do the most damage
            let enemies = match group.side {
                Side::Reindeer => &mut is,
                Side::Infection => &mut rs,
            };


         
         
            let group_count = counts[group.id];


              
            let mut choice = None;
            for (i, e) in enemies.iter().enumerate() 
            {
                //calc the damage group can do to e
                let damage = group.calc_damage( group_count, e );

                if damage > 0 {
                    match choice 
                    {
                        Some((t, d, _)) => {
                            if damage > d {
                                choice = Some((e, damage, i));
                            }
                            else if damage == d {
                                let e_pow = e.effective_power(counts[e.id()]);
                                let t_pow = t.effective_power(counts[t.id()]);
                                if  e_pow > t_pow {
                                    choice = Some((e,damage, i));
                                }
                                else if e_pow == t_pow{
                                    if e.initiative() > t. initiative() {
                                        choice = Some((e,damage, i));
                                    }
                                }
                            }
                        },
                        None => choice = Some((e, damage, i)),
                    }
                }
                       
            }
            


           if let Some(thing) = choice {
                //calculate the target which will take most damage
                //and the damage amount
                //store that if can damage anything 
                let target_index = thing.2;
                let pot_damage = thing.1;
                let target_id = thing.0.id();
              
                if pot_damage > 0
                {

                 //   println!("{:?} Group {} is targeting group {} for pot_damage {} ", group.side, group.id(), target_id, pot_damage);
                //select enemy to which it would deal the most damage then effective power then initiative
                //let target = group;
                   enemies.remove(target_index);
                    target_map.insert( group.id(), target_id );
                }
           }
           
        } 

        if target_map.is_empty() 
        {
            was_draw = true;
            break;
        }
        //println!();
        //attack phase
        //sort groups
        all.sort_by( |g1,g2| g2.initiative().cmp(&g1.initiative()));
        for group in &all{
            if counts[group.id] > 0 {

               if let Some(target_id) = target_map.get(&group.id())
               {
                   let t = all.iter().find(|g| {g.id() == *target_id} ).unwrap();
                   let damage = group.calc_damage(counts[group.id()], t);
                   let units_killed = cmp::min( counts[*target_id], damage / t.hit_points);
                   //println!("{:?} Group {} is attacking group {} for damage {} killing {} units", group.side, group.id(), target_id, damage, units_killed);
                   counts[ *target_id ] -= units_killed; 
               }
                //find the target for the group 
                //attack the targeted group if there is one
                //target->take_damage(group);
                //target.apply_damage();
            }
                   
        }
            
        //println!();    
                
        //remove dead groups
        all.retain(|g| counts[g.id] > 0 );
        
    }


    if !was_draw && all.first().unwrap().side == Side::Reindeer 
    {
        if boost_step == 1 {
          let unit_count = all.iter().fold(0, |v, g| v + counts[g.id()]);
            println!("Remaining units = {}", unit_count);
            break;
        }
        else
        {
            println!("Reindeer won with boost of {}", boost);
            boost_step /= 2;
            boost = min_boost + boost_step;
        }
    }
    else
    {
        println!("Reindeer lost with boost of {}", boost);
        min_boost = boost;
        boost += boost_step;
    }
    }

  
}