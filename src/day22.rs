


use std::collections::{HashMap,BinaryHeap};
use std::collections::hash_map::Entry;
use std::cmp::Ordering;

// 
/*
const DEPTH : usize = 510;
const TX : usize = 10;
const TY : usize = 10;
// */
// /*
const DEPTH : usize = 7863;
const TX : usize = 14;
const TY : usize = 760;
//  */
const MAX_X :usize = TX +1000;
const MAX_Y :usize = TY +1000;



pub fn answer1()
{
    let mut risk = 0;

    let mut el = vec![0; (TX + 1) * (TY + 1)];
    let index = |a,b| { a + b * (TX +1)};

    for x in 0..=TX {
        for y in 0..=TY {

            let gi = 
            match (x,y) {

                (0,0)=>0,
                (TX,TY)=>0,
                (0,c)=>c*48271,
                (r,0)=>r*16807,
                (a,b)=>el[ index(a-1,b) ] * el[ index(a,b-1) ],

            };

            let erosion_level = (gi + DEPTH) % 20183;
            el[ index(x,y) ] =  erosion_level;

            risk += erosion_level % 3;
        }
    } 

    println!("risk = {}", risk);
}



#[derive(PartialEq, Eq, Copy, Clone, Hash)]
enum Equipped
{
    Torch,
    Gear,
    Nothing,
}

type Pos = (usize, usize);

#[derive(PartialEq, Eq, Copy, Clone, Hash)]
struct Node
{
    pos: Pos,
    equipped: Equipped,
}

#[derive(PartialEq, Eq, Copy, Clone)]
struct PriorityNode
{
    node: Node,
    priority: usize,
}
// The priority queue depends on `Ord`.
// Explicitly implement the trait so the queue becomes a min-heap
// instead of a max-heap.
impl Ord for PriorityNode {
    fn cmp(&self, other: &PriorityNode) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other.priority.cmp(&self.priority)
         
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for PriorityNode {
    fn partial_cmp(&self, other: &PriorityNode) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

 

fn gen_neigbours(current: &Node, terrain: &[u8])->Vec<Node>
{
    let mut neighbours = Vec::new();

    //possiblities are move NESW or change equipment
    //pos cant go negative or >= MAX_X MAX_Y 
    let index = |p:(usize,usize)| { p.0 + p.1 * MAX_X };

    //get current terrain type
    let t_index = index(current.pos);
    let t_type = terrain[t_index];

    //get current equipment
    let current_equipment = current.equipped;

    //calc invalid terrain to move to
    //calc valid equipment to move to
    let ( invalid_terrain, valid_equipment) = match (t_type, current_equipment) {
        (0, Equipped::Torch)     => (1, Equipped::Gear),
        (0, Equipped::Gear)      => (2, Equipped::Torch),
        (1, Equipped::Gear)      => (2, Equipped::Nothing),
        (1, Equipped::Nothing)   => (0, Equipped::Gear),
        (2, Equipped::Torch)     => (1, Equipped::Nothing),
        (2, Equipped::Nothing)   => (0, Equipped::Torch),
        _=> { panic!();  },
    };
        

    let mut test_and_add_pos = |pos:(usize,usize)| {
        //get next terrain type
        let next_terrain = terrain[ index(pos) ];
        //if != invalid move add to neighbours
        if next_terrain != invalid_terrain {
            neighbours.push( Node{pos, equipped: current_equipment});
        }
    };

    //for each direction add if valid 
    if current.pos.0 > 0  {
        let next_pos = ( current.pos.0 -1, current.pos.1);
        test_and_add_pos( next_pos );       
    }

    if current.pos.0 < MAX_X -1  {
        let next_pos = ( current.pos.0 +1, current.pos.1);
        test_and_add_pos( next_pos );       
    }

    if current.pos.1 > 0  {
        let next_pos = ( current.pos.0, current.pos.1 -1);
        test_and_add_pos( next_pos );       
    }

    if current.pos.1 < MAX_Y -1  {
        let next_pos = ( current.pos.0, current.pos.1 +1);
        test_and_add_pos( next_pos );       
    }

    //can always change equipment
    neighbours.push( Node{pos:current.pos, equipped: valid_equipment});
    //add equipment change move
    //can move from RT to R or N or change to Climbing Gear  invalid terrain = W
    //can move from RC to R or W or change to Torch          invalid terrain = N
    //can move from NT to N or R or change to Nothing        invalid         = W
    //can mvoe from NN to N or W or change to Torch                          = R
    //can move from WC to W or R or change to Nothing                        = N
    //can move from WC to W or N or change to Torch                          = R

    neighbours
}
fn heuristic(node: &Node, target: &Node)->usize
{
    //manhattan distance between nodes
    //ignore the equipment for now

    let x_distance = if node.pos.0 > target.pos.0 { node.pos.0 - target.pos.0}else{target.pos.0 - node.pos.0};
    let y_distance = if node.pos.1 > target.pos.1 { node.pos.1 - target.pos.1}else{target.pos.1 - node.pos.1};
    let e_distance = if node.equipped == target.equipped {0} else {7};
    x_distance + y_distance + e_distance
}

fn move_cost(current: &Node, next: &Node)->usize{
    
    //assume only valid steps are generated 
    //assume 1 or 7
    if current.equipped == next.equipped {
        1
    }
    else{
        7
    }
}

fn print_terrain(terrain: &[u8], width: usize)
{
    for chunk in terrain.chunks(width) {
        let mut line = String::new();
        for v in chunk {
            line.push( match v {
                0 => '.',
                1 => '=',
                2 => '|',
                3 => '*',
                _ => {panic!();},
            });
        }
        println!("{}", line);
    }
    println!();
}

pub fn answer2()
{
    let mut el : Vec<usize> = vec![0; MAX_X * MAX_Y ];
    let mut terrain : Vec<u8> = vec![0; MAX_X * MAX_Y ];
 
    let index = |a,b| { a + b * MAX_X };

    for x in 0..MAX_X {
        for y in 0..MAX_Y {

            let gi =  
            match (x,y) {

                (0,0)=>0,
                (TX,TY)=>0,
                (0,c)=>c*48271,
                (r,0)=>r*16807,
                (a,b)=>el[ index(a-1,b) ] * el[ index(a,b-1) ],

            };

            let erosion_level = (gi + DEPTH) % 20183;
            el[ index(x,y) ] =  erosion_level;

            terrain[index(x,y)] = (erosion_level % 3) as u8;
        }
    } 

/*
    print_terrain( &terrain , MAX_X);
   // let terrain = terrain;
    let mut tcopy = [terrain.clone(), terrain.clone(), terrain.clone()];
// */

    //search for quickest route
    //look at 15
    //a*

    let mut frontier = BinaryHeap::new();
    let mut came_from = HashMap::new();
    let mut cost_so_far = HashMap::new();

    let start = Node{pos:(0,0), equipped: Equipped::Torch};
    
    frontier.push( PriorityNode{node:start, priority:0});
    came_from.insert( start, None);
    cost_so_far.insert( start, 0);

    let target = Node{pos:(TX, TY), equipped: Equipped::Torch};
 

    while !frontier.is_empty() {

        let current = frontier.pop().unwrap().node;

        if current == target {
            break;
        } 
/*
        let e_index = match current.equipped {
            Equipped::Torch => {println!("Torch"); 0},
            Equipped::Gear => {println!("Gear");1},
            Equipped::Nothing => {println!("None");2},
        };
        
        tcopy[e_index][ index(current.pos.0, current.pos.1)] = 3;
        print_terrain( &tcopy[e_index], MAX_X);
       // */

        let neighbors = gen_neigbours(&current, &terrain);
        for node in neighbors {
            let new_cost = cost_so_far[&current] + move_cost(&current, &node);

           
            let add_to_search = match  cost_so_far.entry(node) {
                Entry::Occupied(mut o)  =>{
                    if new_cost < *o.get() {
                        o.insert(new_cost);
                        true
                    }
                    else{
                        false
                    }
                },
                Entry::Vacant(v)=>{
                    v.insert(new_cost);
                    true
                },
               
            };
            if add_to_search {
                    let priority = new_cost + heuristic(&node, &target);
                    frontier.push(PriorityNode{node, priority});
                    came_from.insert( node, Some(current) );
            }

            /*
            if node is not in cost_so_far or new cost < cost_so_far[next] {
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(target, next)
                frontier.push(Priority{node, priority});
                came_from[next] = current
            }
            let priority = 
            */
        }
    }

  let total_cost = cost_so_far[&target];
/*
    let pindex = |p:(usize,usize)| { p.0 + p.1 * MAX_X };
  

    let mut t2 = terrain.clone();
    let mut current = target;
    t2[pindex(target.pos)] = 3;
    while let Some(n) = came_from[&current] {
        current = n;
        t2[pindex(n.pos)] = 3;
    }

    print_terrain(&t2, MAX_X);
    //
    */
/*
frontier = PriorityQueue()
frontier.put(start, 0)
came_from = {}
cost_so_far = {}
came_from[start] = None
cost_so_far[start] = 0

while not frontier.empty():
   current = frontier.get()

   if current == goal:
      break
   
   for next in graph.neighbors(current):
      new_cost = cost_so_far[current] + graph.cost(current, next)
      if next not in cost_so_far or new_cost < cost_so_far[next]:
         cost_so_far[next] = new_cost
         priority = new_cost + heuristic(goal, next)
         frontier.put(next, priority)
         came_from[next] = current
*/
    //print route cost
    println!("cost = {}", total_cost);
}