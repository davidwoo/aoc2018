

extern crate regex;

use input;
use self::regex::Regex;
use std::str;
use std::ops::Index;
use std::ops::IndexMut;



enum Clay
{
    Row( (usize,usize), usize),
    Col( usize, (usize,usize)),
}



struct Grid{

    items : Vec<char>,
    x_min : usize,
    x_max : usize,
    y_min : usize,
    y_max : usize,
    
    x_range : usize,
    y_range : usize,
}



impl Grid{
    fn new(deposits: Vec<Clay>)->Grid {


    let mut min_y = usize::max_value();
    let mut max_y = usize::min_value();
    let mut min_x = usize::max_value();
    let mut max_x = usize::min_value();
    //find min max y
    //find min max x
    for dep in &deposits{
        match dep {
            Clay::Row( (x0, x1) , y) => {
                if *y < min_y {
                    min_y = *y;
                }        
                if *y > max_y {
                    max_y = *y;
                } 

                if *x0 < min_x{
                    min_x = *x0;
                }
                if *x1 > max_x{
                    max_x = *x1;
                }
            },
            Clay::Col( x ,(y0, y1)) => {
                if *x < min_x {
                    min_x = *x;
                }        
                if *x > max_x {
                    max_x = *x;
                } 

                if *y0 < min_y{
                    min_y = *y0;
                }
                if *y1 > max_y{
                    max_y = *y1;
                }
            },
        }
    }

 

    //allocate a vector
    min_x -= 5;
    max_x += 5;
    let x_range = max_x - min_x + 1;
    let y_range = max_y+ 1;

    let mut grid = vec!['.';x_range * y_range];

    //x - xmin + y * x_range
    //fill in grid
    for dep in &deposits {
        match dep {
            Clay::Row( (x0, x1) , y) => {
               for c in *x0..=*x1 {
                   let index = y * x_range + c - min_x;
                   grid[ index ]= '#';
               }
            },
            Clay::Col( x ,(y0, y1)) => {
                for r in *y0..=*y1 {
                  let index = r * x_range + x - min_x;
                   grid[ index ]= '#';
                }
            },
        }
    }

    grid[500-min_x] = '+';



        Grid{
            items: grid,
            x_min : min_x,
            x_max : max_x,
            x_range : x_range,
            y_min : min_y,
            y_max : max_y,
            y_range : y_range,
        }
    }

    fn print(&self)
    {
        println!("");
        for line in self.items.chunks(self.x_range) {
            println!("{}", line.into_iter().collect::<String>() );
        }
    }

    fn print_range(&self,start:usize, count: usize)
    {
        println!("");
        for line in self.items.chunks(self.x_range).skip(start).take(count) {
            println!("{}", line.into_iter().collect::<String>() );
        }
    }

    fn get(&self, coord:(usize,usize))->Option<char>{
        if coord.0 >= self.x_min && 
            coord.0 <= self.x_max && 
            coord.1 <= self.y_max {

                Some( self[coord] )
        }
        else {
            None
        }
    }

    fn is_supported(&self, coord:(usize,usize))->bool{
        match self.get((coord.0, coord.1+1))
        {
            Some(c) if c=='#' || c=='~' =>{
                true
            },
            _=>{false},
        }
    }

    fn find_left_block_column(&self, coord:(usize,usize))->Option<usize>{
        
        //keep moving left until we find a '#'
        let mut col = coord.0;

      
        loop {
            match self.get((col-1,coord.1)) {
                Some('#')=>{
                    return Some(col);
                },

                Some('|')=>{
                    col -= 1;
                },
               
                _=>return None,

            }

        }
        
    }
}

impl Index<(usize,usize)> for Grid {
    type Output = char;

    fn index(&self, coord:(usize,usize))->&char{
        &self.items[ coord.0 - self.x_min + coord.1 * self.x_range]
    }

}
impl IndexMut<(usize,usize)> for Grid {

    fn index_mut(&mut self, coord:(usize,usize))->&mut char{
        &mut self.items[ coord.0 - self.x_min + coord.1 * self.x_range]
    }

}

#[derive(PartialEq,Copy,Clone)]
enum State{
    Dropping,
    Left,
    Right,
   // RightFromLeft((usize,usize)),
    Finished,
}

struct PathPoint
{
    pos: (usize, usize),
    state : State
}

struct Simulation
{
    grid : Grid,
    current: (usize, usize),
    state: State,
    stack: Vec<PathPoint>,
    wet_count: i32,
    tilda_count: i32,
}

impl Simulation{
    fn new(grid: Grid, start: (usize, usize) )->Simulation{
        Simulation{
            grid,
            current: start,
            state: State::Dropping,
            stack: Vec::new(),
            wet_count: 0,
            tilda_count: 0,
        }
    }
    fn is_finished(&self)->bool{
        self.state == State::Finished
    }

    fn set_state(&mut self, next_state: State)
    {
        //on exit

        self.state = next_state;

        //on enter
        match self.state {
            State::Left=>{
                    //only push if there is no water to the right
                let right = (self.current.0+1,self.current.1);
                match self.grid.get(right) {
                    Some( _c ) if _c != '|' =>{
                        self.stack.push( PathPoint{pos:self.current,state: State::Right} );
                    },
                    _=>(),
                }
            },
            _=>(),
            
        }
    }

    fn set_wet(&mut self, next:(usize,usize))
    {
        
       //mark grid | wet  
        //assert_eq!(self.grid[next],'.');
if self.grid[next] == '.' {
        self.grid[next] = '|';

        if next.1 >= self.grid.y_min && next.1 <= self.grid.y_max {
            self.wet_count += 1;
        }
        else{
            println!("hello!");
        }
}
else{
    //println!("curios");
        let bob = if self.current.1 < 25 {0} else {self.current.1-25};
        self.grid.print_range( bob, 50);
}
        
    }

    fn on_dropping(&mut self)
    {
        let next = (self.current.0,self.current.1+1);
        match self.grid.get( next ) {
            Some(c) if c == '~' || c== '#' =>{
                    //then supported below
                    //change state move left
                       
                self.set_state(State::Left);

            },
            Some('.')=>{

                //push current pos + L on stack
                if self.current.1 >= self.grid.y_min {
                    self.stack.push( PathPoint{pos:self.current,state: State::Left} );
                } 

                //move down
                self.current = next;
            
              
                //if test(add 1 to counter if between min and max values)
                self.set_wet(next);

              
            },

            None | Some(_) =>{ //stop exploring that way
                self.jump_back_or_finish();

            }
        }
    }

    fn jump_back_or_finish(&mut self)
    {
        while !self.stack.is_empty() {
                        
            match self.stack.pop().unwrap() {
                PathPoint{pos, state:next} => {

                    if self.grid.is_supported(pos) {
                        self.current=pos;
                        self.set_state(next);
                        return;
                    }
                      
                },
            }
        }
        
        self.set_state( State::Finished );
    }


    fn on_left(&mut self)
    {
        let next = (self.current.0-1, self.current.1);

        match self.grid.get(next) {

            Some(c) if c == '~' || c== '#' => { //then blcked left

                //change state to moving right from left
                //jump back to stored right most state (pop stack)
                //let right_state = State::RightFromLeft(self.current);
                //self.set_state(  right_state );
                
                assert!(!self.stack.is_empty());
                match self.stack.pop().unwrap() {
                    PathPoint{pos, state}=>{
                            self.current=pos;
                            self.set_state( state );                                   
                    },
                }
                
            },

            None => {self.jump_back_or_finish()},
            Some('|') => {self.jump_back_or_finish()}, //stop exploring that way


            Some(_)=> {//move left

                self.current = next;
            
                //mark grid | wet  
                self.set_wet(next);
            

                if !self.grid.is_supported(self.current) 
                {
                    self.set_state( State::Dropping );
                   
                }
                
            }  
        }
    }


    fn on_right(&mut self) {

        let next = (self.current.0+1, self.current.1);

        match self.grid.get(next) {

            Some('#') => { //then blcked right
               

                //find the # to the left after only |'s
                //mark grid between xleft and xright as ~
                if let Some( left ) = self.grid.find_left_block_column(self.current){
                    for x in left..=self.current.0 {
                        self.grid[(x,self.current.1)] = '~';
                        self.tilda_count += 1;
                    }
                }

               self.jump_back_or_finish()
            },

            None => {self.jump_back_or_finish()},
            Some('|') | Some('~') => {
                    panic!();
                    self.jump_back_or_finish();
                }, //stop exploring that way


            Some(_)=> {//move right

                self.current = next;
            
                //mark grid | wet  
                self.set_wet(next);
   

                if !self.grid.is_supported(self.current) 
                {
                    self.set_state( State::Dropping );
                }
                
            }  
        }
    }


    fn update(&mut self)
    {

        match self.state {
            State::Dropping =>{
                self.on_dropping();

            },
            State::Left=>{
                self.on_left();
           
            },
            State::Right=>{
                self.on_right()
                },

            State::Finished=>(),
        }


     //   let bob = if self.current.1 < 25 {0} else {self.current.1-25};
     //   self.grid.print_range( bob, 50);
    }
}

pub fn answer1()
{
    //read in lines
    let lines = input::read_lines("data/day17.txt");

    let col_pattern = Regex::new(r"^x=([0-9]+), y=([0-9]+)..([0-9]+)$").unwrap();
    let row_pattern = regex::Regex::new(r"^y=([0-9]+), x=([0-9]+)..([0-9]+)$").unwrap();


    let mut deposits = Vec::new();
    deposits.reserve( lines.len());
    for line in lines{
        if let Some(m) = col_pattern.captures(&line) {

            deposits.push( Clay::Col{
             0: m.get(1).unwrap().as_str().parse::<usize>().unwrap(),
             1: (
            m.get(2).unwrap().as_str().parse::<usize>().unwrap(), 
            m.get(3).unwrap().as_str().parse::<usize>().unwrap()
            )} );
        }
        else if let Some(m) = row_pattern.captures(&line){
            deposits.push( Clay::Row{
              0:   (
             m.get(2).unwrap().as_str().parse::<usize>().unwrap(),
             
            m.get(3).unwrap().as_str().parse::<usize>().unwrap()
            ), 
            1 : m.get(1).unwrap().as_str().parse::<usize>().unwrap()
            } );
        }
    }


    let mut grid = Grid::new(deposits);

    //grid[(498,2)]='.';
    //grid.print();
  
    //starting at 500,0
    let mut simulation = Simulation::new(grid, (500,0));
    while !simulation.is_finished() {
        simulation.update();
    }
   
    println!("Count is {}, ~={}", simulation.wet_count, simulation.tilda_count);
}
