
use input;
use std::mem;

type Grid = Vec<Vec<char>>;

struct Wood
{
    acres : [ Grid; 2],
    num_columns : usize,
    num_rows : usize,
    read_index : usize,
    write_index : usize,
}

impl Wood{
    fn new( lines: Vec<String>)->Wood{
        
        let num_columns = lines[0].len() + 2;
        let num_rows = lines.len() + 2;

        let mut acres = [vec![], vec![]];
      
        acres[0].reserve(num_rows);
      
  
        for _ in 0..num_rows {
            acres[0].push( vec!['.'; num_columns]);
        
        }

        for (r,line) in lines.into_iter().enumerate() {
            for (c, obj) in line.chars().enumerate() {
                if r >= num_rows-1 {
                    println!("bllo");
                }
                if c >= num_columns -1{
                    println!("bllo");
                }
                acres[0][r+1][c+1]=obj;
            }
        }

        acres[1] = acres[0].clone();

        Wood{
            acres,
            num_columns,
            num_rows,
            read_index: 0,
            write_index: 1,
        }
    }

    fn print(&self)
    {
         println!("");
        for row in self.acres[self.read_index].iter().skip(1).take(self.num_rows-2) {
            let row_str = row.iter().skip(1).take(self.num_columns-2).collect::<String>();

            println!("{}", row_str);
        }
        
    }

    fn next_acre( &self, row: usize, col: usize) -> char
    {
        //count the 8 sites around 
        let mut tree_count = 0;
        let read_acres =  &self.acres[self.read_index];
        for ra in -1..=1 as i32 {
            for ca in -1..=1 as i32 {
                if ra == 0 && ca == 0 {
                    continue;
                }
                let r = row as i32 + ra;
                let c = col as i32 + ca;
                if read_acres[r as usize][c as usize] == '|' {
                    tree_count += 1;
                }
                if tree_count >= 3 {
                    return '|';
                }
            }
        }

        //nothing happens
        self.acres[self.read_index][row][col]
    }

    fn next_tree( &self, row: usize, col: usize) -> char
    {
        //count the 8 sites around 
        let mut yard_count = 0;
        let read_acres =  &self.acres[self.read_index];
        for ra in -1..=1 as i32 {
            for ca in -1..=1 as i32{
                if ra == 0 && ca == 0 {
                    continue;
                }
                let r = row as i32 + ra;
                let c = col as i32 + ca;
                if read_acres[r as usize][c as usize] == '#' {
                    yard_count += 1;
                }
                if yard_count >= 3 {
                    return '#';
                }
            }
        }

        //nothing happens
        self.acres[self.read_index][row][col]
    }

     fn next_yard( &self, row: usize, col: usize) -> char
    {
        //count the 8 sites around 
        let mut yard_count = 0;
        let mut tree_count = 0;
        let read_acres =  &self.acres[self.read_index];
        for ra in -1..=1 as i32 {
            for ca in -1..=1 as i32{
                if ra == 0 && ca == 0 {
                    continue;
                }
                let r = row as i32 + ra;
                let c = col as i32 + ca;
                match read_acres[r as usize][c as usize] {
                    '#'=>{ yard_count += 1;},
                    '|'=>{ tree_count += 1;},
                    _=>()
                }
                if yard_count >= 1 && tree_count >= 1 {
                    return '#';
                }
            }
        }

        '.'
    }

    fn next(&self, row: usize, col: usize) -> char
    {
        match self.acres[self.read_index][row][col] {

            '.'=>{ self.next_acre(row, col) },
            '|'=>{ self.next_tree(row, col) },
            '#'=>{ self.next_yard(row, col) },
            _=>'.',
        }
    }

    fn update(&mut self)
    {
        for r in 1..=self.num_rows-2 {
            for c in 1..=self.num_columns-2{


                self.acres[self.write_index][r][c] = self.next(r,c);
            }
        }

        mem::swap(&mut self.write_index, &mut self.read_index);
    }

    fn calc_resources(&self)->usize
    {
        let read_acres =  &self.acres[self.read_index];

        let mut tree_count = 0;
        let mut yard_count = 0;
        for r in 1..=self.num_rows-2 {
            for c in 1..=self.num_columns-2{
                match read_acres[r][c] {
                    '#'=>{yard_count+=1;},
                    '|'=>{tree_count+=1;},
                    _=>(),
                }
            }
        }

        tree_count * yard_count
    }
}

pub fn answer1()
{
    let lines = input::read_lines("data/day18.txt");

   
    let mut wood = Wood::new(lines);

    for i in 0..100000 {
        wood.update();

        if i % 1000 == 999{
            //wood.print();
            let resource_value = wood.calc_resources();
            println!("Gen : {} Resources = {}", i, resource_value);
        }
    }

    let resource_value = wood.calc_resources();
    println!("Resources = {}", resource_value);
}

pub fn answer2()
{
    let numbers = [
        220785,
        202722,
        190568,
        211552,
        212711,
        193960,
        196860,
    ];

    let fred = 1000000 % 7;

    println!("The answer is {}", numbers[fred]);
}